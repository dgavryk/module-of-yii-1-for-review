<?php namespace payments\controllers;

use payments\models\active_records\Payment;
use payments\models\forms\payments\PaymentFormModel;
use payments\models\Payments;
use payments\models\Wallets;

class PaymentsController extends BaseController
{

    /**
     * @return array
     */
    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['list', 'create', 'edit', 'show'],
                'expression' => function() {
                    return in_array(\UsersPermissions::getCurrentUserRoleId(), [\User::ROLE_SELLER, \User::ROLE_ADMIN, \User::ROLE_BUYER]);
                },
            ],
            [
                'allow',
                'actions' => ['getPaymentsList', 'getPaymentData', 'getActiveWalletData', 'getWalletData'],
                'expression' => function() {
                    return in_array(\UsersPermissions::getCurrentUserRoleId(), [\User::ROLE_SELLER, \User::ROLE_ADMIN]);
                },
            ],
            [
                'allow',
                'actions' => ['savePaymentData', 'deletePayment'],
                'expression' => function() {
                    return in_array(\UsersPermissions::getCurrentUserRoleId(true), [\User::ROLE_ADMIN]);
                },
            ],
            ['deny'],
        ];
    }

    /**
     * Action show list of existing forms
     */
    public function actionList()
    {
        if ($this->isShowedWarning([\User::ROLE_SELLER, \User::ROLE_ADMIN], \AccessWarnings::WARNING_DENIED_ACCESS, false, true)) {
            return;
        }

        $this->render('list');
    }

    /**
     * Ajax get list of payments
     */
    public function actionGetPaymentsList()
    {
        $requestFiltersEncoded = \Yii::app()->request->getParam('filters');
        $perPage        = \Yii::app()->request->getParam('perPage');
        $pageNumber     = \Yii::app()->request->getParam('pageNumber');

        $filters = [];
        if (!empty($requestFiltersEncoded)) {
            $requestFilters = json_decode($requestFiltersEncoded, true);
            if (is_array($requestFilters)) {
                !empty($requestFilters['payDateRangeFrom']) && $filters['pay_date_from'] = $requestFilters['payDateRangeFrom'];
                !empty($requestFilters['payDateRangeTo']) && $filters['pay_date_to'] = $requestFilters['payDateRangeTo'];
                !empty($requestFilters['sellerId']) && $filters['seller_id'] = $requestFilters['sellerId'];
                !empty($requestFilters['paymentMethodNameId']) && $filters['payment_method_name_id'] = $requestFilters['paymentMethodNameId'];
                !empty($requestFilters['currencyId']) && $filters['currency_id'] = $requestFilters['currencyId'];
                !empty($requestFilters['status']) && $filters['status'] = $requestFilters['status'];
            }
        }

        $payments = new Payments();
        $paymentsListData = $payments->getPaymentsList($filters, $perPage, $pageNumber);

        $responseCode = 200;
        $responseData = [
            'paymentsList'      => $paymentsListData['listData'],
            'paymentsTotalRow'  => $paymentsListData['totalItemsRow'],
            'totalItemsCount'   => $paymentsListData['totalItemsCount'],
        ];

        $this->sendResponse($responseCode, $responseData);
    }

    /**
     * Action Add new payment
     */
    public function actionCreate()
    {
        if ($this->isShowedWarning([\User::ROLE_ADMIN], \AccessWarnings::WARNING_DENIED_ACCESS, true, true)) {
            return;
        }

        $formModel = new PaymentFormModel('create');

        $this->render('edit', [
            'formModel'     => $formModel,
        ]);
    }

    /**
     * Action Edit payment
     */
    public function actionEdit()
    {
        if ($this->isShowedWarning([\User::ROLE_ADMIN], \AccessWarnings::WARNING_DENIED_ACCESS, true, true)) {
            return;
        }

        $paymentId = \Yii::app()->request->getParam('paymentId');

        if (!empty($paymentId)) {
            $payments = new Payments();
            $payment = $payments->getPaymentById($paymentId, true, false, true);

            if (!empty($payment) && $payment->status == Payment::STATUS_NOT_PAID) {
                $formModel = new PaymentFormModel('update');
                $formModel->loadData($payment);
            }
        }

        if (isset($formModel) && !empty($formModel->paymentId)) {
            $this->render('edit', [
                'formModel' => $formModel,
            ]);
        } else {
            $this->renderAccessWarning(\AccessWarnings::WARNING_WRONG_REQUEST);
        }
    }

    /**
     * Action Show payment
     */
    public function actionShow()
    {
        if ($this->isShowedWarning([\User::ROLE_ADMIN, \User::ROLE_SELLER], \AccessWarnings::WARNING_DENIED_ACCESS, true, true)) {
            return;
        }

        $paymentId = \Yii::app()->request->getParam('paymentId');

        $formModel  = null;
        $walletData = [];

        if (!empty($paymentId)) {
            $payments = new Payments();
            $isCheckAccess = (\UsersPermissions::getCurrentUserRoleId(true) != \User::ROLE_ADMIN);
            $payment = $payments->getPaymentById($paymentId, true, $isCheckAccess, true);

            if (!empty($payment)) {
                $wallets = new Wallets();
                $walletData = $wallets->getWalletData($payment->wallet_id);

                if (!empty($walletData)) {
                    $formModel = new PaymentFormModel();
                    $formModel->loadData($payment);
                }
            }
        }

        if (isset($formModel) && !empty($formModel->paymentId)) {
            $this->render('show', [
                'formModel'     => $formModel,
                'walletData'    => $walletData,
            ]);
        } else {
            $this->renderAccessWarning(\AccessWarnings::WARNING_WRONG_REQUEST);
        }
    }

    /**
     * Ajax create/edit payment
     */
    public function actionSavePaymentData()
    {
        $responseCode = 500;
        $responseData = [
            'error_title' => \Yii::t('all', 'error'),
            'error_message' => \Yii::t('all', 'saved_error'),
            'errors' => [],
        ];

        $savedData  = \Yii::app()->request->getParam('data', []);
        $paymentId  = \Yii::app()->request->getParam('paymentId');

        if (!empty($paymentId)) {
            $payments = new Payments();
            $isCheckAccess = (\UsersPermissions::getCurrentUserRoleId(true) != \User::ROLE_ADMIN);
            $payment = $payments->getPaymentById($paymentId, true, $isCheckAccess, true);
        } else {
            $payment = new Payment();
        }

        if (!empty($payment)) {
            $formModel = new PaymentFormModel((!empty($payment->id) ? 'update' : 'create'));
            $formModel->setData($savedData, $payment->id);
            if ($formModel->validate() && $formModel->saveData()) {
                $responseCode = 200;
                if (empty($payment->id)) {
                    $responseData = [
                        'redirect_url' => '/payments/payments/list'
                    ];
                    \Yii::app()->user->setFlash('savedSuccess', \Yii::t('all', 'payment_created_successful'));
                } else {
                    $responseData = [
                        'success_title' => \Yii::t('all', 'success'),
                        'success_message' => \Yii::t('all', 'saved_successful'),
                    ];
                }
            } else {
                $responseData['errors'] = $formModel->getErrors();
            }
        }

        $this->sendResponse($responseCode, $responseData);
    }

    /**
     * Ajax get payment data
     */
    public function actionGetPaymentData()
    {
        $responseCode = 500;
        $responseData = [
            'error_title' => \Yii::t('all', 'error'),
            'error_message' => \Yii::t('all', 'error_receiving_data'),
        ];

        $paymentId = \Yii::app()->request->getParam('paymentId');

        if (!empty($paymentId)) {
            $payments = new Payments();
            $isCheckAccess = (\UsersPermissions::getCurrentUserRoleId(true) != \User::ROLE_ADMIN);
            $payment = $payments->getPaymentById($paymentId, true, $isCheckAccess, true);

            if (!empty($payment)) {
                $formModel = new PaymentFormModel('update');
                $formModel->loadData($payment);

                $responseCode = 200;
                $responseData = [
                    'paymentData' => $formModel->getData(),
                ];
            }
        }

        $this->sendResponse($responseCode, $responseData);
    }

    /**
     * Ajax Get active wallet by user and currency
     */
    public function actionGetActiveWalletData()
    {
        $responseCode = 500;
        $responseData = [
            'error_title' => \Yii::t('all', 'error'),
            'error_message' => \Yii::t('all', 'error_receiving_data'),
        ];

        $sellerId   = \Yii::app()->request->getParam('seller_id');
        $currencyId = \Yii::app()->request->getParam('currency_id');

        if (!empty($sellerId) && !empty($currencyId) && (\UsersPermissions::getCurrentUserRoleId(true) == \User::ROLE_ADMIN || \UsersPermissions::getCurrentUserId(true) == $sellerId)) {
            $wallets = new Wallets();

            $responseCode = 200;
            $responseData = [
                'walletData' => $wallets->getUserActiveWallet($sellerId, $currencyId),
            ];
        }

        $this->sendResponse($responseCode, $responseData);
    }

    /**
     * Ajax Get wallet data by id
     */
    public function actionGetWalletData()
    {
        $responseCode = 500;
        $responseData = [
            'error_title' => \Yii::t('all', 'error'),
            'error_message' => \Yii::t('all', 'error_receiving_data'),
        ];

        $walletId = \Yii::app()->request->getParam('wallet_id');

        if (!empty($walletId)) {
            $wallets = new Wallets();

            $isCheckAccess = (\UsersPermissions::getCurrentUserRoleId(true) != \User::ROLE_ADMIN);
            $wallet = $wallets->getWalletById($walletId, true, $isCheckAccess, false);

            if (!empty($wallet)) {
                $responseCode = 200;
                $responseData = [
                    'walletData' => $wallets->getWalletData($wallet->id),
                ];
            }
        }

        $this->sendResponse($responseCode, $responseData);
    }

    /**
     * Ajax delete wallet
     */
    public function actionDeletePayment()
    {
        $responseCode = 500;
        $responseData = [
            'error_title' => \Yii::t('all', 'error'),
            'error_message' => \Yii::t('all', 'error_delete_payment'),
        ];

        $paymentId  = \Yii::app()->request->getParam('paymentId');

        $payments = new Payments();
        $isCheckAccess = (\UsersPermissions::getCurrentUserRoleId(true) != \User::ROLE_ADMIN);
        $payment = $payments->getPaymentById($paymentId, true, $isCheckAccess, true);

        if (!empty($payment->id)) {
            $formModel = new PaymentFormModel('delete');
            $formModel->loadData($payment);

            if ($formModel->validate() && $formModel->deleteData()) {
                $responseCode = 200;
                $responseData = [
                    'success_title' => \Yii::t('all', 'deleted_successful'),
                    'success_message' => \Yii::t('all', 'payment_was_delete_successful'),
                ];
            }
        }

        $this->sendResponse($responseCode, $responseData);
    }

}