<?php namespace payments\controllers;

use payments\models\PaymentsMethods;

class MethodsController extends BaseController
{

    /**
     * @return array
     */
    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['list'],
                'expression' => function() {
                    return in_array(\UsersPermissions::getCurrentUserRoleId(), [\User::ROLE_SELLER, \User::ROLE_ADMIN, \User::ROLE_BUYER]);
                },
            ],
            [
                'allow',
                'actions' => ['getPaymentMethodsList'],
                'expression' => function() {
                    return in_array(\UsersPermissions::getCurrentUserRoleId(), [\User::ROLE_SELLER]);
                },
            ],
            ['deny'],
        ];
    }

    /**
     * Action show list of existing payment methods
     */
    public function actionList()
    {
        if ($this->isShowedWarning([\User::ROLE_SELLER], \AccessWarnings::WARNING_ONLY_SELLER, false, true)) {
            return;
        }

        $this->render('list');
    }

    /**
     * Ajax get list of payment methods
     */
    public function actionGetPaymentMethodsList()
    {
        $paymentsMethods = new PaymentsMethods();
        $methodsList = $paymentsMethods->getAllPaymentMethods();

        $responseCode = 200;
        $responseData = ['methodsList' => $methodsList];

        $this->sendResponse($responseCode, $responseData);
    }


}