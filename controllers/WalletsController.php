<?php namespace payments\controllers;

use payments\models\active_records\Wallet;
use payments\models\forms\wallets\WalletFormModel;
use payments\models\PaymentsMethods;
use payments\models\Wallets;

class WalletsController extends BaseController
{

    /**
     * @return array
     */
    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['list', 'create', 'edit'],
                'expression' => function() {
                    return in_array(\UsersPermissions::getCurrentUserRoleId(), [\User::ROLE_SELLER, \User::ROLE_ADMIN, \User::ROLE_BUYER]);
                },
            ],
            [
                'allow',
                'actions' => ['saveWalletData', 'getWalletData', 'getWalletsList', 'deleteWallet', 'activateWallet'],
                'expression' => function() {
                    return in_array(\UsersPermissions::getCurrentUserRoleId(), [\User::ROLE_SELLER]);
                },
            ],
            ['deny'],
        ];
    }

    /**
     * Action show list of existing forms
     */
    public function actionList()
    {
        if ($this->isShowedWarning([\User::ROLE_SELLER], \AccessWarnings::WARNING_ONLY_SELLER, false, true)) {
            return;
        }

        $this->render('list');
    }

    /**
     * Ajax get list of wallets
     */
    public function actionGetWalletsList()
    {
        $currencyId = \Yii::app()->request->getParam('currencyId');
        $status     = \Yii::app()->request->getParam('status');

        $filters = [];
        !empty($currencyId)     && $filters['currency_id'] = $currencyId;
        !empty($status)         && $filters['status'] = $status;

        $wallets = new Wallets();
        $walletsList = $wallets->getWalletsList($filters);

        $responseCode = 200;
        $responseData = ['walletsList' => $walletsList];

        $this->sendResponse($responseCode, $responseData);
    }

    /**
     * Action Add new wallet for seller
     */
    public function actionCreate()
    {
        if ($this->isShowedWarning([\User::ROLE_SELLER], \AccessWarnings::WARNING_ONLY_SELLER, false, true)) {
            return;
        }

        $paymentMethodId = \Yii::app()->request->getParam('paymentMethodId');

        $paymentsMethods = new PaymentsMethods();
        $paymentMethod = $paymentsMethods->getPaymentMethodById($paymentMethodId);

        if (!empty($paymentMethod)) {
            $wallet = new Wallet();

            $formModel = new WalletFormModel('create');
            $formModel->setData(['payment_method_id' => $paymentMethod->id]);

            $this->render('edit', [
                'paymentMethod' => $paymentMethod,
                'wallet'        => $wallet,
                'formModel'     => $formModel,
            ]);
        } else {
            $this->renderAccessWarning(\AccessWarnings::WARNING_WRONG_REQUEST);
        }
    }

    /**
     * Action Edit wallet for seller
     */
    public function actionEdit()
    {
        if ($this->isShowedWarning([\User::ROLE_SELLER], \AccessWarnings::WARNING_ONLY_SELLER, false, true)) {
            return;
        }

        $walletId = \Yii::app()->request->getParam('walletId');

        $wallets = new Wallets();
        $wallet = $wallets->getWalletById($walletId, true, true, true); /** @var \payments\models\active_records\Wallet $wallet */

        if (!empty($wallet)) {

            $paymentsMethods = new PaymentsMethods();
            $paymentMethod = $paymentsMethods->getPaymentMethodById($wallet->payment_method_id, true, true);

            if (!empty($paymentMethod)) {

                $formModel = new WalletFormModel('update');
                $formModel->loadData($wallet);

                $this->render('edit', [
                    'paymentMethod'     => $paymentMethod,
                    'wallet'            => $wallet,
                    'formModel'         => $formModel,
                ]);
            } else {
                throw new \Exception('Wrong payment method for wallet #' . $wallet->id);
            }
        } else {
            $this->renderAccessWarning(\AccessWarnings::WARNING_WRONG_REQUEST);
        }
    }

    /**
     * Action Create/Edit wallet
     */
    public function actionSaveWalletData()
    {
        $responseCode = 500;
        $responseData = [
            'error_title' => \Yii::t('all', 'error'),
            'error_message' => \Yii::t('all', 'saved_error'),
            'errors' => [],
        ];

        $savedData  = \Yii::app()->request->getParam('data', []);
        $walletId   = \Yii::app()->request->getParam('walletId');

        if (!empty($walletId)) {
            $wallets = new Wallets();
            $wallet = $wallets->getWalletById($walletId, true, true, true);
        } else {
            $wallet = new Wallet();
        }

        if (!empty($wallet)) {
            $formModel = new WalletFormModel((!empty($wallet->id) ? 'update' : 'create'));
            $formModel->setData($savedData, $wallet->id);
            if ($formModel->validate() && $formModel->saveData()) {
                $responseCode = 200;
                if (empty($wallet->id)) {
                    $responseData = [
                        'redirect_url' => '/payments/wallets/list'
                    ];
                    \Yii::app()->user->setFlash('savedSuccess', \Yii::t('all', 'wallet_created_successful'));
                } else {
                    $responseData = [
                        'success_title' => \Yii::t('all', 'success'),
                        'success_message' => \Yii::t('all', 'saved_successful'),
                    ];
                }
            } else {
                $responseData['errors'] = $formModel->getErrors();
            }
        }

        $this->sendResponse($responseCode, $responseData);
    }

    /**
     * Action get wallet data
     */
    public function actionGetWalletData()
    {
        $responseCode = 500;
        $responseData = [
            'error_title' => \Yii::t('all', 'error'),
            'error_message' => \Yii::t('all', 'error_receiving_data'),
        ];

        $walletId   = \Yii::app()->request->getParam('walletId');

        $wallets = new Wallets();
        $wallet = $wallets->getWalletById($walletId, true, true, true); /** @var \payments\models\active_records\Wallet $wallet */

        if (!empty($wallet->id)) {
            $formModel = new WalletFormModel('update');
            $formModel->loadData($wallet);
            $walletData = $formModel->getData();

            $responseCode = 200;
            $responseData = [
                'walletData' => $walletData
            ];
        }

        $this->sendResponse($responseCode, $responseData);
    }

    /**
     * Ajax delete wallet
     */
    public function actionDeleteWallet()
    {
        $responseCode = 500;
        $responseData = [
            'error_title' => \Yii::t('all', 'error'),
            'error_message' => \Yii::t('all', 'error_delete_wallet'),
        ];

        $walletId   = \Yii::app()->request->getParam('walletId');

        $wallets = new Wallets();
        $wallet = $wallets->getWalletById($walletId, true, true, true); /** @var \payments\models\active_records\Wallet $wallet */

        if (!empty($wallet->id)) {
            $formModel = new WalletFormModel('delete');
            $formModel->loadData($wallet);

            if ($formModel->validate() && $formModel->deleteData()) {
                $responseCode = 200;
                $responseData = [
                    'success_title' => \Yii::t('all', 'deleted_successful'),
                    'success_message' => \Yii::t('all', 'wallet_was_delete_successful'),
                ];
            }
        }

        $this->sendResponse($responseCode, $responseData);
    }

    /**
     * Ajax activate wallet
     */
    public function actionActivateWallet()
    {
        $responseCode = 500;
        $responseData = [
            'error_title' => \Yii::t('all', 'error'),
            'error_message' => \Yii::t('all', 'error_change_status_wallet'),
        ];

        $walletId   = \Yii::app()->request->getParam('walletId');

        $wallets = new Wallets();
        $wallet = $wallets->getWalletById($walletId, true, true, true); /** @var \payments\models\active_records\Wallet $wallet */

        if (!empty($wallet->id)) {
            $formModel = new WalletFormModel('updateStatus');
            $formModel->loadData($wallet);
            $formModel->status = Wallet::STATUS_ACTIVE;

            if ($formModel->validate() && $formModel->updateStatusData()) {
                $responseCode = 200;
                $responseData = [
                    'success_title' => \Yii::t('all', 'updated_successful'),
                    'success_message' => \Yii::t('all', 'wallet_status_changed_successful'),
                ];
            }
        }

        $this->sendResponse($responseCode, $responseData);
    }

}