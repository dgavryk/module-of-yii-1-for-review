<?php namespace payments\controllers;

use payments\models\active_records\Invoice;
use payments\models\forms\invoices\InvoiceFormModel;
use payments\models\Invoices;

class InvoicesController extends BaseController
{

    /**
     * @return array
     */
    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            [
                'allow',
                'actions' => ['list', 'create', 'edit', 'show'],
                'expression' => function() {
                    return in_array(\UsersPermissions::getCurrentUserRoleId(), [\User::ROLE_SELLER, \User::ROLE_ADMIN, \User::ROLE_BUYER]);
                },
            ],
            [
                'allow',
                'actions' => ['getInvoicesList', 'getInvoiceData'],
                'expression' => function() {
                    return in_array(\UsersPermissions::getCurrentUserRoleId(), [\User::ROLE_BUYER, \User::ROLE_ADMIN]);
                },
            ],
            [
                'allow',
                'actions' => ['saveInvoiceData', 'deleteInvoice'],
                'expression' => function() {
                    return in_array(\UsersPermissions::getCurrentUserRoleId(true), [\User::ROLE_ADMIN]);
                },
            ],
            ['deny'],
        ];
    }

    /**
     * Action show list of existing invoices
     */
    public function actionList()
    {
        if ($this->isShowedWarning([\User::ROLE_BUYER, \User::ROLE_ADMIN], \AccessWarnings::WARNING_DENIED_ACCESS, false, true)) {
            return;
        }

        $this->render('list');
    }

    /**
     * Ajax get list of invoices
     */
    public function actionGetInvoicesList()
    {
        $requestFiltersEncoded = \Yii::app()->request->getParam('filters');
        $perPage        = \Yii::app()->request->getParam('perPage');
        $pageNumber     = \Yii::app()->request->getParam('pageNumber');

        $filters = [];
        if (!empty($requestFiltersEncoded)) {
            $requestFilters = json_decode($requestFiltersEncoded, true);
            if (is_array($requestFilters)) {
                !empty($requestFilters['payDateRangeFrom']) && $filters['pay_date_from'] = $requestFilters['payDateRangeFrom'];
                !empty($requestFilters['payDateRangeTo']) && $filters['pay_date_to'] = $requestFilters['payDateRangeTo'];
                !empty($requestFilters['buyerId']) && $filters['buyer_id'] = $requestFilters['buyerId'];
                !empty($requestFilters['paymentMethodNameId']) && $filters['payment_method_name_id'] = $requestFilters['paymentMethodNameId'];
                !empty($requestFilters['currencyId']) && $filters['currency_id'] = $requestFilters['currencyId'];
                !empty($requestFilters['status']) && $filters['status'] = $requestFilters['status'];
            }
        }

        $invoices = new Invoices();
        $invoicesListData = $invoices->getInvoicesList($filters, $perPage, $pageNumber);

        $responseCode = 200;
        $responseData = [
            'invoicesList'      => $invoicesListData['listData'],
            'invoicesTotalRow'  => $invoicesListData['totalItemsRow'],
            'totalItemsCount'   => $invoicesListData['totalItemsCount'],
        ];

        $this->sendResponse($responseCode, $responseData);
    }

    /**
     * Action Add new invoice
     */
    public function actionCreate()
    {
        if ($this->isShowedWarning([\User::ROLE_ADMIN], \AccessWarnings::WARNING_DENIED_ACCESS, true, true)) {
            return;
        }

        $formModel = new InvoiceFormModel('create');

        $this->render('edit', [
            'formModel'     => $formModel,
        ]);
    }

    /**
     * Action Edit invoice
     */
    public function actionEdit()
    {
        if ($this->isShowedWarning([\User::ROLE_ADMIN], \AccessWarnings::WARNING_DENIED_ACCESS, true, true)) {
            return;
        }

        $invoiceId = \Yii::app()->request->getParam('invoiceId');

        if (!empty($invoiceId)) {
            $invoices = new Invoices();
            $invoice = $invoices->getInvoiceById($invoiceId, true, false, true);

            if (!empty($invoice) && $invoice->status == Invoice::STATUS_NOT_PAID) {
                $formModel = new InvoiceFormModel('update');
                $formModel->loadData($invoice);
            }
        }

        if (isset($formModel) && !empty($formModel->invoiceId)) {
            $this->render('edit', [
                'formModel' => $formModel,
            ]);
        } else {
            $this->renderAccessWarning(\AccessWarnings::WARNING_WRONG_REQUEST);
        }
    }

    /**
     * Action Show invoice
     */
    public function actionShow()
    {
        if ($this->isShowedWarning([\User::ROLE_ADMIN, \User::ROLE_SELLER], \AccessWarnings::WARNING_DENIED_ACCESS, true, true)) {
            return;
        }

        $invoiceId = \Yii::app()->request->getParam('invoiceId');

        $formModel  = null;

        if (!empty($invoiceId)) {
            $invoices = new Invoices();
            $isCheckAccess = (\UsersPermissions::getCurrentUserRoleId(true) != \User::ROLE_ADMIN);
            $invoice = $invoices->getInvoiceById($invoiceId, true, $isCheckAccess, true);

            if (!empty($invoice)) {
                $formModel = new InvoiceFormModel();
                $formModel->loadData($invoice);
            }
        }

        if (isset($formModel) && !empty($formModel->invoiceId)) {
            $this->render('show', [
                'formModel'     => $formModel,
            ]);
        } else {
            $this->renderAccessWarning(\AccessWarnings::WARNING_WRONG_REQUEST);
        }
    }

    /**
     * Ajax create/edit payment
     */
    public function actionSaveInvoiceData()
    {
        $responseCode = 500;
        $responseData = [
            'error_title' => \Yii::t('all', 'error'),
            'error_message' => \Yii::t('all', 'saved_error'),
            'errors' => [],
        ];

        $savedData  = \Yii::app()->request->getParam('data', []);
        $invoiceId  = \Yii::app()->request->getParam('invoiceId');

        if (!empty($invoiceId)) {
            $invoices = new Invoices();
            $isCheckAccess = (\UsersPermissions::getCurrentUserRoleId(true) != \User::ROLE_ADMIN);
            $invoice = $invoices->getInvoiceById($invoiceId, true, $isCheckAccess, true);
        } else {
            $invoice = new Invoice();
        }

        if (!empty($invoice)) {
            $formModel = new InvoiceFormModel((!empty($invoice->id) ? 'update' : 'create'));
            $formModel->setData($savedData, $invoice->id);
            if ($formModel->validate() && $formModel->saveData()) {
                $responseCode = 200;
                if (empty($invoice->id)) {
                    $responseData = [
                        'redirect_url' => '/payments/invoices/list'
                    ];
                    \Yii::app()->user->setFlash('savedSuccess', \Yii::t('all', 'invoice_created_successful'));
                } else {
                    $responseData = [
                        'success_title' => \Yii::t('all', 'success'),
                        'success_message' => \Yii::t('all', 'saved_successful'),
                    ];
                }
            } else {
                $responseData['errors'] = $formModel->getErrors();
            }
        }

        $this->sendResponse($responseCode, $responseData);
    }

    /**
     * Ajax get invoice data
     */
    public function actionGetInvoiceData()
    {
        $responseCode = 500;
        $responseData = [
            'error_title' => \Yii::t('all', 'error'),
            'error_message' => \Yii::t('all', 'error_receiving_data'),
        ];

        $invoiceId  = \Yii::app()->request->getParam('invoiceId');

        if (!empty($invoiceId)) {
            $invoices = new Invoices();
            $isCheckAccess = (\UsersPermissions::getCurrentUserRoleId(true) != \User::ROLE_ADMIN);
            $invoice = $invoices->getInvoiceById($invoiceId, true, $isCheckAccess, true);

            if (!empty($invoice)) {
                $formModel = new InvoiceFormModel('update');
                $formModel->loadData($invoice);

                $responseCode = 200;
                $responseData = [
                    'invoiceData' => $formModel->getData(),
                ];
            }
        }

        $this->sendResponse($responseCode, $responseData);
    }

    /**
     * Ajax delete invoice
     */
    public function actionDeleteInvoice()
    {
        $responseCode = 500;
        $responseData = [
            'error_title' => \Yii::t('all', 'error'),
            'error_message' => \Yii::t('all', 'error_delete_invoice'),
        ];

        $invoiceId  = \Yii::app()->request->getParam('invoiceId');

        $invoices = new Invoices();
        $isCheckAccess = (\UsersPermissions::getCurrentUserRoleId(true) != \User::ROLE_ADMIN);
        $invoice = $invoices->getInvoiceById($invoiceId, true, $isCheckAccess, true);

        if (!empty($invoice->id)) {
            $formModel = new InvoiceFormModel('delete');
            $formModel->loadData($invoice);

            if ($formModel->validate() && $formModel->deleteData()) {
                $responseCode = 200;
                $responseData = [
                    'success_title' => \Yii::t('all', 'deleted_successful'),
                    'success_message' => \Yii::t('all', 'invoice_was_delete_successful'),
                ];
            }
        }

        $this->sendResponse($responseCode, $responseData);
    }

}