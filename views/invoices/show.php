<?php
/**
 * @var payments\controllers\InvoicesController $this
 * @var payments\models\forms\invoices\InvoiceFormModel $formModel
 */
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('main_menu', 'invoices');
$this->breadcrumbs = [
    Yii::t('main_menu', 'invoices'),
];

$this->includeAngularFile('payments/invoices/show.js');
?>

<div class="form col-md-12" ng-controller="PaymentsInvoicesShowCtrl">

    <div ng-init="init(
                <?php echo (!empty($formModel->invoiceId) ? $formModel->invoiceId : 'null' ) ?>
            )"></div>

    <div class="panel panel-default">

        <div class="panel-heading">
            <h2><strong><?php echo Yii::t('all', 'view_invoice_details') ?></strong></h2>
        </div>

        <div class="panel-body">
            <!-- PAYMENT DATA -->
            <div class="col-md-6 form-horizontal">
                <div>
                    <!-- BUYER -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'buyer') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->getBuyerName(); ?>
                            </div>
                        </div>
                    </div>
                    <!-- CURRENCY -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'currency') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->getCurrencyName(); ?>
                            </div>
                        </div>
                    </div>
                    <!-- PAYMENT METHOD -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'payment_method') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->getPaymentMethodName(); ?>
                            </div>
                        </div>
                    </div>
                    <!-- PAY DATE -->
                    <div class="row form-group">
                        <label class="col-sm-3 control-label text-center"><?= Yii::t('all', 'pay_period') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->payDate; ?>
                            </div>
                        </div>
                    </div>
                    <!-- PAY PERIOD -->
                    <div class="row form-group">
                        <label class="col-sm-3 control-label text-center"><?= Yii::t('all', 'pay_period') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->payPeriodFrom . ' - ' . $formModel->payPeriodTo; ?>
                            </div>
                        </div>
                    </div>
                    <!-- PAID -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'paid') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->invoiceSum; ?>
                            </div>
                        </div>
                    </div>
                    <!-- FEE -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'fee') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->feeSum; ?>
                            </div>
                        </div>
                    </div>
                    <!-- RECEIVED -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'received') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->receivedSum; ?>
                            </div>
                        </div>
                    </div>
                    <!-- STATUS -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'status') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php
                                $statusesList = \payments\models\active_records\Payment::getStatusList(true);
                                echo isset($statusesList[$formModel->status]) ? $statusesList[$formModel->status] : '';
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- COMMENT -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'comment') ?>:</label>
                        <div class="col-md-9">
                            <div style="margin-top: 7px;"><?php echo $formModel->comment ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAYMENT DATA -->

            <br />

            <div class="col-md-12">
                <div class="row col-md-offset-5">
                    <button type="button"
                            class="btn btn-sm btn-default"
                            ng-click="cancelButtonClick()"
                        >
                        <?php echo Yii::t('all', 'back'); ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>