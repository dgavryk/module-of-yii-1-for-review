<?php
/**
 * @var payments\controllers\InvoicesController $this
 * @var payments\models\forms\invoices\InvoiceFormModel $formModel
 */
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('main_menu', 'invoices');
$this->breadcrumbs = [
    Yii::t('main_menu', 'invoices'),
];

$this->includeAngularFile('payments/invoices/edit.js');
?>

<div class="form col-md-12" ng-controller="PaymentsInvoicesEditCtrl">

    <div ng-init="init(
                <?php echo (!empty($formModel->invoiceId) ? $formModel->invoiceId : 'null' ) ?>
            )"></div>

    <div class="panel panel-default">

        <div class="panel-heading">
            <h2><strong><?php echo (!empty($formModel->invoiceId)
                        ? Yii::t('all', 'create_new_invoice')
                        : Yii::t('all', 'edit_invoice'))
                    ?></strong></h2>
        </div>

        <div class="panel-body">
            <!-- INVOICE DATA -->
            <div class="col-md-6 form-horizontal">
                <div ng-show="!isShowProcessDataLoader" ng-cloak>
                    <!-- BUYER -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'buyer') ?>:</label>
                        <div class="col-md-6">
                            <!-- show on CREATE invoice -->
                            <select class="form-control"
                                    ng-model="formData.userId"
                                    ng-class="{'error': formErrors.userId}"
                                    ng-show="!invoiceId"
                                >
                                <?php
                                $buyers = new Buyers();
                                $buyersList = $buyers->getBuyersSimple();
                                foreach($buyersList as $buyerData) { ?>
                                    <option value="<?php echo $buyerData['id'] ?>"><?php echo $buyerData['name'] ?></option>
                                <?php } ?>
                            </select>
                            <!-- show on UPDATE payment -->
                            <div style="margin-top: 7px;" ng-show="invoiceId">
                                <span ng-bind="formData.userName"></span>
                            </div>
                            <div class="errorMessage" ng-if="formErrors.userId"><div ng-bind="formErrors.userId[0]"></div></div>
                        </div>
                    </div>
                    <!-- CURRENCY -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'currency') ?>:</label>
                        <div class="col-md-6">
                            <!-- show on CREATE invoice -->
                            <select class="form-control"
                                    ng-model="formData.currencyId"
                                    ng-class="{'error': formErrors.currencyId}"
                                    ng-show="!invoiceId"
                                >
                                <?php
                                $currencies = Currencies::getCurrenciesSimpleList();
                                foreach($currencies as $id => $name) { ?>
                                    <option value="<?php echo $id ?>"><?php echo $name ?></option>
                                <?php } ?>
                            </select>
                            <!-- show on UPDATE payment -->
                            <div style="margin-top: 7px;" ng-show="invoiceId">
                                <span ng-bind="formData.currencyName"></span>
                            </div>
                            <div class="errorMessage" ng-if="formErrors.currencyId"><div ng-bind="formErrors.currencyId[0]"></div></div>
                        </div>
                    </div>
                    <!-- PAYMENT METHOD -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'payment_method') ?>:</label>
                        <div class="col-md-6">
                            <!-- show on CREATE invoice -->
                            <select class="form-control"
                                    ng-model="formData.paymentMethodNameId"
                                    ng-class="{'error': formErrors.paymentMethodNameId}"
                                    ng-show="!invoiceId"
                                >
                                <?php
                                $paymentMethodNames = \payments\models\PaymentMethodsNames::getAllPaymentMethodNames();
                                foreach($paymentMethodNames as $v) { ?>
                                    <option value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
                                <?php } ?>
                            </select>
                            <!-- show on UPDATE payment -->
                            <div style="margin-top: 7px;" ng-show="invoiceId">
                                <span ng-bind="formData.paymentMethodName"></span>
                            </div>
                            <div class="errorMessage" ng-if="formErrors.paymentMethodNameId"><div ng-bind="formErrors.paymentMethodNameId[0]"></div></div>
                        </div>
                    </div>
                    <!-- PAY DATE -->
                    <div class="row form-group">
                        <label class="col-sm-3 control-label text-center"><?= Yii::t('all', 'pay_period') ?>:</label>
                        <div class="col-sm-9">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class='input-group date' id='payDatePicker'>
                                        <input type='text' value="" class="form-control date-input-field readonly-active" readonly="readonly" ng-class="{'error': formErrors.payDate}" />
                                                            <span class="input-group-addon input-group-addon-small-padding">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 errorMessage" ng-if="formErrors.payDate"><div ng-bind="formErrors.payDate[0]"></div></div>
                        </div>
                    </div>
                    <!-- PAY PERIOD -->
                    <div class="row form-group">
                        <label class="col-sm-3 control-label text-center"><?= Yii::t('all', 'pay_period') ?>:</label>
                        <div class="col-sm-9">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class='input-group date' id='payPeriodFromPicker'>
                                        <input type='text' value="" class="form-control date-input-field readonly-active" readonly="readonly" ng-class="{'error': formErrors.payPeriodFrom}" />
                                                            <span class="input-group-addon input-group-addon-small-padding">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-sm-1 text-center">-</div>-->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class='input-group date' id='payPeriodToPicker'>
                                        <input type='text' value="" class="form-control date-input-field readonly-active" readonly="readonly" ng-class="{'error': formErrors.payPeriodTo}" />
                                                            <span class="input-group-addon input-group-addon-small-padding">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                    </div>
                                </div>
                            </div>
                            <div class="errorMessage text-center" ng-if="formErrors.payPeriod"><div ng-bind="formErrors.payPeriod[0]"></div></div>
                        </div>
                    </div>
                    <!-- INVOICE -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'invoice') ?>:</label>
                        <div class="col-md-6">
                            <input type="text"
                                   placeholder="0.00"
                                   class="form-control"
                                   ng-model="formData.invoiceSum"
                                   ng-class="{'error': formErrors.invoiceSum}"
                                />
                            <div class="errorMessage" ng-if="formErrors.invoiceSum"><div ng-bind="formErrors.invoiceSum[0]"></div></div>
                        </div>
                    </div>
                    <!-- FEE -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'fee') ?>:</label>
                        <div class="col-md-6">
                            <input type="text"
                                   placeholder="0.00"
                                   class="form-control"
                                   ng-model="formData.feeSum"
                                   ng-class="{'error': formErrors.feeSum}"
                                />
                            <div class="errorMessage" ng-if="formErrors.feeSum"><div ng-bind="formErrors.feeSum[0]"></div></div>
                        </div>
                    </div>
                    <!-- RECEIVED -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'received') ?>:</label>
                        <div class="col-md-6">
                            <input type="text"
                                   placeholder="0.00"
                                   class="form-control"
                                   ng-model="formData.receivedSum"
                                   ng-class="{'error': formErrors.receivedSum}"
                                />
                            <div class="errorMessage" ng-if="formErrors.receivedSum"><div ng-bind="formErrors.receivedSum[0]"></div></div>
                        </div>
                    </div>
                    <!-- STATUS -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'status') ?>:</label>
                        <div class="col-md-6">
                            <select class="form-control" ng-model="formData.status" ng-class="{'error': formErrors.status}">
                                <?php
                                $statuses = \payments\models\active_records\Invoice::getStatusList();
                                foreach($statuses as $id => $name) { ?>
                                    <option value="<?php echo $id ?>"><?php echo $name ?></option>
                                <?php } ?>
                            </select>
                            <div class="errorMessage" ng-if="formErrors.status"><div ng-bind="formErrors.status[0]"></div></div>
                        </div>
                    </div>
                    <!-- COMMENT -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'comment') ?>:</label>
                        <div class="col-md-9">
                            <textarea class="form-control"
                                      ng-model="formData.comment"
                                      ng-class="{'error': formErrors.comment}"
                                      style="resize: none;"
                                      rows="7"
                                ></textarea>
                            <div class="errorMessage" ng-if="formErrors.comment"><div ng-bind="formErrors.comment[0]"></div></div>
                        </div>
                    </div>
                </div>

                <?php $this->renderPartial('//_components/loader/mainLoader', ['id' => 'processDataLoader']); ?>
            </div>
            <!-- END INVOICE DATA -->

            <br />

            <div class="col-md-12">
                <div class="row col-md-offset-5">
                    <button type="button"
                            class="btn btn-sm btn-primary"
                            ng-click="saveDataClick()"
                        >
                        <i class="fa fa-dot-circle-o"></i> <?php echo (!empty($formModel->invoiceId) ? Yii::t('all', 'save') : Yii::t('all', 'add')); ?>
                    </button>
                    <button type="button"
                            class="btn btn-sm btn-default"
                            ng-click="cancelButtonClick()"
                        >
                        <?php echo Yii::t('all', 'back'); ?>
                    </button>
                </div>
            </div>
        </div>

    </div>

</div>