<?php
/**
 * @var payments\controllers\PaymentsController $this
 * @var payments\models\forms\payments\PaymentFormModel $formModel
 */
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('main_menu', 'payments');
$this->breadcrumbs = [
    Yii::t('main_menu', 'payments'),
];

$this->includeAngularFile('payments/payments/edit.js');
?>

<div class="form col-md-12" ng-controller="PaymentsPaymentsEditCtrl">

    <div ng-init="init(
            <?php echo (!empty($formModel->paymentId) ? $formModel->paymentId : 'null' ) ?>
        )"></div>

    <div class="panel panel-default">

        <div class="panel-heading">
            <h2><strong><?php echo (!empty($formModel->paymentId)
                        ? Yii::t('all', 'create_new_payment')
                        : Yii::t('all', 'edit_payment'))
                    ?></strong></h2>
        </div>

        <div class="panel-body">
            <!-- PAYMENT DATA -->
            <div class="col-md-6 form-horizontal">
                <div ng-show="!isShowProcessDataLoader" ng-cloak>
                    <!-- WEBMASTER -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'seller') ?>:</label>
                        <div class="col-md-6">
                            <!-- show on CREATE payment -->
                            <select class="form-control"
                                    ng-model="formData.userId"
                                    ng-change="reloadWallet()"
                                    ng-class="{'error': formErrors.userId}"
                                    ng-show="!paymentId"
                                >
                                <?php
                                $sellers = new Sellers();
                                $sellersList = $sellers->getSellersSimple();
                                foreach($sellersList as $sellerData) { ?>
                                    <option value="<?php echo $sellerData['id'] ?>"><?php echo $sellerData['name'] ?></option>
                                <?php } ?>
                            </select>
                            <!-- show on UPDATE payment -->
                            <div style="margin-top: 7px;" ng-show="paymentId">
                                <span ng-bind="formData.userName"></span>
                            </div>
                            <div class="errorMessage" ng-if="formErrors.userId"><div ng-bind="formErrors.userId[0]"></div></div>
                        </div>
                    </div>
                    <!-- CURRENCY -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'currency') ?>:</label>
                        <div class="col-md-6">
                            <!-- show on CREATE payment -->
                            <select class="form-control"
                                    ng-model="formData.currencyId"
                                    ng-change="reloadWallet()"
                                    ng-class="{'error': formErrors.currencyId}"
                                    ng-show="!paymentId"
                                >
                                <?php
                                $currencies = Currencies::getCurrenciesSimpleList();
                                foreach($currencies as $id => $name) { ?>
                                    <option value="<?php echo $id ?>"><?php echo $name ?></option>
                                <?php } ?>
                            </select>
                            <!-- show on UPDATE payment -->
                            <div style="margin-top: 7px;" ng-show="paymentId">
                                <span ng-bind="formData.currencyName"></span>
                            </div>
                            <div class="errorMessage" ng-if="formErrors.currencyId"><div ng-bind="formErrors.currencyId[0]"></div></div>
                        </div>
                    </div>
                    <!-- PAYMENT METHOD -->
                    <div class="row form-group" ng-show="walletData.id">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'payment_method') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;"><div ng-bind="walletData.payment_method_name"></div></div>
                        </div>
                    </div>
                    <!-- PAY DATE -->
                    <div class="row form-group">
                        <label class="col-sm-3 control-label text-center"><?= Yii::t('all', 'pay_period') ?>:</label>
                        <div class="col-sm-9">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class='input-group date' id='payDatePicker'>
                                        <input type='text' value="" class="form-control date-input-field readonly-active" readonly="readonly" ng-class="{'error': formErrors.payDate}" />
                                                <span class="input-group-addon input-group-addon-small-padding">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                    </div>
                                </div>
                            </div>
                            <div class="errorMessage" ng-if="formErrors.payDate"><div ng-bind="formErrors.payDate[0]"></div></div>
                        </div>
                    </div>
                    <!-- PAY PERIOD -->
                    <div class="row form-group">
                        <label class="col-sm-3 control-label text-center"><?= Yii::t('all', 'pay_period') ?>:</label>
                        <div class="col-sm-9">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class='input-group date' id='payPeriodFromPicker'>
                                        <input type='text' value="" class="form-control date-input-field readonly-active" readonly="readonly" ng-class="{'error': formErrors.payPeriodFrom}" />
                                                <span class="input-group-addon input-group-addon-small-padding">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-sm-1 text-center">-</div>-->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class='input-group date' id='payPeriodToPicker'>
                                        <input type='text' value="" class="form-control date-input-field readonly-active" readonly="readonly" ng-class="{'error': formErrors.payPeriodTo}" />
                                                <span class="input-group-addon input-group-addon-small-padding">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                    </div>
                                </div>
                            </div>
                            <div class="errorMessage text-center" ng-if="formErrors.payPeriod"><div ng-bind="formErrors.payPeriod[0]"></div></div>
                        </div>
                    </div>
                    <!-- PAID -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'paid') ?>:</label>
                        <div class="col-md-6">
                            <input type="text"
                                   placeholder="0.00"
                                   class="form-control"
                                   ng-model="formData.paidSum"
                                   ng-class="{'error': formErrors.paidSum}"
                                />
                            <div class="errorMessage" ng-if="formErrors.paidSum"><div ng-bind="formErrors.paidSum[0]"></div></div>
                        </div>
                    </div>
                    <!-- FEE -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'fee') ?>:</label>
                        <div class="col-md-6">
                            <input type="text"
                                   placeholder="0.00"
                                   class="form-control"
                                   ng-model="formData.feeSum"
                                   ng-class="{'error': formErrors.feeSum}"
                                />
                            <div class="errorMessage" ng-if="formErrors.feeSum"><div ng-bind="formErrors.feeSum[0]"></div></div>
                        </div>
                    </div>
                    <!-- RECEIVED -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'received') ?>:</label>
                        <div class="col-md-6">
                            <input type="text"
                                   placeholder="0.00"
                                   class="form-control"
                                   ng-model="formData.receivedSum"
                                   ng-class="{'error': formErrors.receivedSum}"
                                />
                            <div class="errorMessage" ng-if="formErrors.receivedSum"><div ng-bind="formErrors.receivedSum[0]"></div></div>
                        </div>
                    </div>
                    <!-- STATUS -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'status') ?>:</label>
                        <div class="col-md-6">
                            <select class="form-control" ng-model="formData.status" ng-class="{'error': formErrors.status}">
                                <?php
                                $statuses = \payments\models\active_records\Payment::getStatusList();
                                foreach($statuses as $id => $name) { ?>
                                    <option value="<?php echo $id ?>"><?php echo $name ?></option>
                                <?php } ?>
                            </select>
                            <div class="errorMessage" ng-if="formErrors.status"><div ng-bind="formErrors.status[0]"></div></div>
                        </div>
                    </div>
                    <!-- COMMENT -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'comment') ?>:</label>
                        <div class="col-md-9">
                            <textarea class="form-control"
                                      ng-model="formData.comment"
                                      ng-class="{'error': formErrors.comment}"
                                      style="resize: none;"
                                      rows="7"
                                ></textarea>
                            <div class="errorMessage" ng-if="formErrors.comment"><div ng-bind="formErrors.comment[0]"></div></div>
                        </div>
                    </div>
                </div>

                <?php $this->renderPartial('//_components/loader/mainLoader', ['id' => 'processDataLoader']); ?>
            </div>
            <!-- END PAYMENT DATA -->

            <!-- WALLET DATA -->
            <div class="col-md-6 form-horizontal">
                <div class="row text-center">
                    <b><?php echo Yii::t('all', 'wallet'); ?></b>
                </div>

                <div ng-show="!isShowWalletDataLoader && walletData.id" ng-cloak>
                    <!-- WALLET ID -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label">
                            <?php echo Yii::t('all', 'id') ?>:
                        </label>
                        <div class="col-md-6" style="margin-top: 7px;">
                            <span ng-bind="walletData.id"></span>
                        </div>
                    </div>
                    <!-- END WALLET ID -->

                    <!-- WALLET NAME -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label">
                            <?php echo Yii::t('all', 'name') ?>:
                        </label>
                        <div class="col-md-6" style="margin-top: 7px;">
                            <span ng-bind="walletData.name"></span>
                        </div>
                    </div>
                    <!-- END WALLET NAME -->

                    <!-- WALLET PAYMENT DATA FIELDS -->
                    <div class="row form-group" ng-repeat="paymentField in walletData.payment_fields">
                        <label class="col-md-3 control-label">
                            <span ng-bind="paymentField.field_name"></span>:
                        </label>
                        <div class="col-md-6" style="margin-top: 7px;">
                            <span ng-bind="paymentField.field_value"></span>
                        </div>
                    </div>
                    <!-- END WALLET PAYMENT DATA FIELDS -->
                </div>

                <div class="row text-center" ng-show="!isShowWalletDataLoader && !walletData.id">
                    <div style="margin-top: 15px;">
                        <span class="h5"><?php echo Yii::t('all', 'active_wallet_for_seller_with_selected_currency_does_not_exist') ?></span>
                    </div>
                </div>

                <?php $this->renderPartial('//_components/loader/mainLoader', ['id' => 'walletDataLoader']); ?>
            </div>
            <!-- END WALLET DATA -->

            <br />

            <div class="col-md-12">
                <div class="row col-md-offset-5">
                    <button type="button"
                            class="btn btn-sm btn-primary"
                            ng-click="saveDataClick()"
                        >
                        <i class="fa fa-dot-circle-o"></i> <?php echo (!empty($formModel->paymentId) ? Yii::t('all', 'save') : Yii::t('all', 'add')); ?>
                    </button>
                    <button type="button"
                            class="btn btn-sm btn-default"
                            ng-click="cancelButtonClick()"
                        >
                        <?php echo Yii::t('all', 'back'); ?>
                    </button>
                </div>
            </div>
        </div>

    </div>

</div>