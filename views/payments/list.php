<?php
/**
 * @var payments\controllers\PaymentsController $this
 */
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('main_menu', 'payments');
$this->breadcrumbs = [
    Yii::t('main_menu', 'payments'),
];

$translates = [
    'confirmDelete' => [
        'title' => \Yii::t('all', 'are_you_sure_want_to_delete_payment'),
        'confirmButton' => \Yii::t('all', 'yes_delete_it'),
        'cancelButton' => \Yii::t('all', 'cancel'),
    ],
    'confirmDeleteAlreadyPaid' => [
        'title' => \Yii::t('all', 'are_you_sure_want_to_delete_payment.user_balance_we_be_recalculated'),
        'confirmButton' => \Yii::t('all', 'yes_delete_it'),
        'cancelButton' => \Yii::t('all', 'cancel'),
    ],
];

$this->includeAngularFile('payments/payments/list.js');
?>

<div class="row" ng-controller="PaymentsPaymentsListCtrl">

    <?php $this->renderPartial('//_components/notifications/saveNotification'); ?>

    <div ng-init="init(
        <?php echo CHtml::encode(json_encode($translates)); ?>
    )"></div>

    <div class="col-md-12" ng-cloak>

        <div>
            <div class="panel panel-default">
                <div class="panel-heading" data-original-title="">
                    <h2><i class="icon-note"></i><span class="break"></span><?php echo Yii::t('main_menu', 'wallets') ?></h2>
                </div>

                <!-- FILTERS -->
                <div class="row">
                    <div class="col-md-12" style="margin-top: 15px;">
                        <div class="row">

                            <!-- PAY DATE PERIOD -->
                            <div class="col-sm-6">
                                <label class="col-sm-3 control-label text-center"><?= Yii::t('all', 'pay_date') ?>:</label>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class='input-group date' id='payDatePickerFrom'>
                                            <input type='text' value="<?php echo date('Y-m-d', time() - 60*60*24*90); ?>" class="form-control date-input-field readonly-active" readonly="readonly" />
                                            <span class="input-group-addon input-group-addon-small-padding">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1">-</div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class='input-group date' id='payDatePickerTo'>
                                            <input type='text' value="<?php echo date('Y-m-d'); ?>" class="form-control date-input-field readonly-active" readonly="readonly" />
                                            <span class="input-group-addon input-group-addon-small-padding">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php if (UsersPermissions::getCurrentUserRoleId(true) == User::ROLE_ADMIN) { ?>
                                <!-- SELLER -->
                                <div class="col-sm-3 col-md-offset-1">
                                    <label class="col-md-4 control-label text-right"><?= Yii::t('all', 'seller') ?>:</label>
                                    <div class="col-md-8">
                                        <select class="form-control" ng-model="filters.sellerId" ng-change="applyFilters()">
                                            <option value="0"><?php echo Yii::t('all', 'all') ?></option>
                                            <?php
                                            $sellers = new Sellers();
                                            $sellersList = $sellers->getSellersSimple();
                                            foreach($sellersList as $sellerData) { ?>
                                                <option value="<?php echo $sellerData['id'] ?>"><?php echo $sellerData['name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <!-- CURRENCY -->
                            <div class="col-sm-3">
                                <label class="col-sm-4 control-label text-right"><?= Yii::t('all', 'currency') ?>:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" ng-model="filters.currencyId" ng-change="applyFilters()">
                                        <option selected="selected" value="0"><?php echo Yii::t('all', 'all') ?></option>
                                        <?php
                                        $currencies = Currencies::getCurrenciesSimpleList();
                                        foreach($currencies as $id => $name) { ?>
                                            <option value="<?php echo $id ?>"><?php echo $name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <!-- STATUS -->
                            <div class="col-sm-3">
                                <label class="col-sm-4 control-label text-right"><?= Yii::t('all', 'status') ?>:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" ng-model="filters.status" ng-change="applyFilters()">
                                        <option selected="selected" value="0"><?php echo Yii::t('all', 'all') ?></option>
                                        <?php
                                        $statusList = \payments\models\active_records\Payment::getStatusList(true);
                                        foreach($statusList as $id => $name) { ?>
                                            <option value="<?php echo $id ?>"><?php echo $name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <!-- PAYMENT METHOD -->
                            <div class="col-sm-4">
                                <label class="col-sm-6 control-label text-right"><?= Yii::t('all', 'payment_method') ?>:</label>
                                <div class="col-sm-6">
                                    <select class="form-control" ng-model="filters.paymentMethodNameId" ng-change="applyFilters()">
                                        <option selected="selected" value="0"><?php echo Yii::t('all', 'all') ?></option>
                                        <?php
                                        $paymentMethodNames = \payments\models\PaymentMethodsNames::getAllPaymentMethodNames();
                                        foreach($paymentMethodNames as $v) { ?>
                                            <option value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FILTERS END -->

                <hr />

                <div class="panel-body">

                    <div class="row text-right">
                        <div class="col-md-offset-10 col-md-2">
                            <span class="btn btn-primary"
                                  ng-click="addPaymentButtonClick()"
                                ><?php echo Yii::t('all', 'add_payment') ?></span>
                        </div>
                    </div>

                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline no-footer" ng-show="paymentsList.length" ng-cloak="">

                        <table class="table table-striped table-bordered dataTable">
                            <thead>
                            <tr>
                                <th class="text-center"><?php echo Yii::t('all', 'id') ?></th>
                                <?php if (UsersPermissions::getCurrentUserRoleId() == User::ROLE_ADMIN) { ?>
                                    <th class="text-center"><?php echo Yii::t('all', 'webmaster') ?></th>
                                <?php } ?>
                                <th class="text-center"><?php echo Yii::t('all', 'currency') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'wallet') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'pay_date') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'pay_period') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'paid') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'fee') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'received') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'payment_method') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'status') ?></th>
                                <?php if (UsersPermissions::getCurrentUserRoleId() == User::ROLE_ADMIN) { ?>
                                    <th class="text-center" width="17%"><?php echo Yii::t('all', 'actions') ?></th>
                                <?php } ?>
                            </tr>
                            </thead>
                            <tr ng-repeat="payment in paymentsList">
                                <td class="text-center"><div ng-bind="payment.id"></div></td>
                                <?php if (UsersPermissions::getCurrentUserRoleId() == User::ROLE_ADMIN) { ?>
                                    <td class="text-center"><div ng-bind="payment.seller_name"></div></td>
                                <?php } ?>
                                <td class="text-center"><div ng-bind="payment.currency_name"></div></td>
                                <td class="text-center"><div ng-bind="payment.wallet_name"></div></td>
                                <td class="text-center"><div ng-bind="payment.pay_date"></div></td>
                                <td class="text-center"><div ng-bind="payment.pay_period_from + ' - ' + payment.pay_period_to"></div></td>
                                <td class="text-center"><div ng-bind="payment.paid_sum"></div></td>
                                <td class="text-center"><div ng-bind="payment.fee_sum"></div></td>
                                <td class="text-center"><div ng-bind="payment.received_sum"></div></td>
                                <td class="text-center"><div ng-bind="payment.payment_method_name"></div></td>
                                <td class="text-center">
                                    <span ng-class="{'red-text': payment.status == <?php echo \payments\models\active_records\Payment::STATUS_NOT_PAID ?>, 'green-text': payment.status == <?php echo \payments\models\active_records\Payment::STATUS_PAID ?>}" >
                                        <div ng-bind="payment.status_name"></div>
                                    </span>
                                </td>
                                <?php if (UsersPermissions::getCurrentUserRoleId(true) == User::ROLE_ADMIN) { ?>
                                    <td class="text-center">
                                        <span class="btn btn-default"
                                              ng-show="payment.status == <?php echo \payments\models\active_records\Payment::STATUS_PAID ?>"
                                              ng-click="showButtonClick(payment.id)"
                                            ><?php echo Yii::t('all', 'show') ?></span>
                                        <span class="btn btn-default"
                                              ng-show="payment.status == <?php echo \payments\models\active_records\Payment::STATUS_NOT_PAID ?>"
                                              ng-click="editButtonClick(payment.id)"
                                            ><?php echo Yii::t('all', 'edit') ?></span>
                                        <span class="btn btn-danger"
                                              ng-click="deleteButtonClick(payment.id, (payment.status == <?php echo \payments\models\active_records\Payment::STATUS_PAID ?>))"
                                            ><?php echo Yii::t('all', 'delete') ?></span>
                                    </td>
                                <?php } ?>
                            </tr>
                        </table>

                        <div class="row" ng-show="filters.currencyId > 0 && paymentsTotalRow.paid_sum">
                            <div class="col-md-12">
                                <span>
                                    <b style="color: darkblue;"><?php echo Yii::t('all', 'total_paid_sum') ?></b>: <b><span ng-bind="paymentsTotalRow.paid_sum"></span></b>;
                                </span>
                                <span>
                                    <b style="color: darkred;"><?php echo Yii::t('all', 'total_fee_sum') ?></b>: <b><span ng-bind="paymentsTotalRow.fee_sum"></span></b>;
                                </span>
                                <span>
                                    <b style="color: darkgreen;"><?php echo Yii::t('all', 'total_received_sum') ?></b>: <b><span ng-bind="paymentsTotalRow.received_sum"></span></b>;
                                </span>
                            </div>
                        </div>

                        <!-- PAGINATION -->
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="dataTables_info"><?= Yii::t('all', 'rows') ?> {{(currentPage - 1) * itemsPerPage + 1}} - {{currentPage * itemsPerPage}} <?= Yii::t('all', 'of') ?> {{totalItems}}</div>
                            </div>
                            <div class="col-xs-6">
                                <div class="dataTables_paginate paging_simple_numbers">
                                    <pagination
                                        total-items="totalItems"
                                        items-per-page="itemsPerPage"
                                        ng-model="currentPage"
                                        max-size="maxSize"
                                        class="paginate_button"
                                        boundary-links="true"
                                        rotate="false"
                                        num-pages="numPages"
                                        ng-change="pageChanged('B')"
                                        previous-text="<?= Yii::t('all', 'prev') ?>"
                                        next-text="<?= Yii::t('all', 'next') ?>"
                                        first-text="<<"
                                        last-text=">>">
                                    </pagination>
                                </div>
                            </div>
                        </div>
                        <!-- PAGINATION END -->

                    </div>

                    <div ng-class="row" ng-show="paymentsList.length <= 0 && !isShowPaymentsListLoader">
                        <div class="text-center" style="margin-top: 30px; margin-bottom: 30px;">
                            <span class="h4"><?php echo Yii::t('all', 'no_payments_found'); ?></span>
                        </div>
                    </div>

                    <?php $this->renderPartial('//_components/loader/mainLoader', ['id' => 'paymentsListLoader']); ?>

                </div>
            </div>
        </div>
    </div>
</div>