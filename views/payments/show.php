<?php
/**
 * @var payments\controllers\PaymentsController $this
 * @var payments\models\forms\payments\PaymentFormModel $formModel
 * @var array $walletData
 */
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('main_menu', 'payments');
$this->breadcrumbs = [
    Yii::t('main_menu', 'payments'),
];

$this->includeAngularFile('payments/payments/show.js');
?>

<div class="form col-md-12" ng-controller="PaymentsPaymentsShowCtrl">

    <div ng-init="init(
                <?php echo (!empty($formModel->paymentId) ? $formModel->paymentId : 'null' ) ?>
            )"></div>

    <div class="panel panel-default">

        <div class="panel-heading">
            <h2><strong><?php echo Yii::t('all', 'view_payment_details') ?></strong></h2>
        </div>

        <div class="panel-body">
            <!-- PAYMENT DATA -->
            <div class="col-md-6 form-horizontal">
                <div>
                    <!-- WEBMASTER -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'seller') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->getSellerName(); ?>
                            </div>
                        </div>
                    </div>
                    <!-- CURRENCY -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'currency') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->getCurrencyName(); ?>
                            </div>
                        </div>
                    </div>
                    <!-- PAYMENT METHOD -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'payment_method') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo isset($walletData['payment_method_name']) ? $walletData['payment_method_name'] : '' ?>
                            </div>
                        </div>
                    </div>
                    <!-- PAY DATE -->
                    <div class="row form-group">
                        <label class="col-sm-3 control-label text-center"><?= Yii::t('all', 'pay_period') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->payDate; ?>
                            </div>
                        </div>
                    </div>
                    <!-- PAY PERIOD -->
                    <div class="row form-group">
                        <label class="col-sm-3 control-label text-center"><?= Yii::t('all', 'pay_period') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->payPeriodFrom . ' - ' . $formModel->payPeriodTo; ?>
                            </div>
                        </div>
                    </div>
                    <!-- PAID -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'paid') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->paidSum; ?>
                            </div>
                        </div>
                    </div>
                    <!-- FEE -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'fee') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->feeSum; ?>
                            </div>
                        </div>
                    </div>
                    <!-- RECEIVED -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'received') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php echo $formModel->receivedSum; ?>
                            </div>
                        </div>
                    </div>
                    <!-- STATUS -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'status') ?>:</label>
                        <div class="col-md-6">
                            <div style="margin-top: 7px;">
                                <?php
                                    $statusesList = \payments\models\active_records\Payment::getStatusList(true);
                                    echo isset($statusesList[$formModel->status]) ? $statusesList[$formModel->status] : '';
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- COMMENT -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label"><?php echo Yii::t('all', 'comment') ?>:</label>
                        <div class="col-md-9">
                            <div style="margin-top: 7px;"><?php echo $formModel->comment ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAYMENT DATA -->

            <!-- WALLET DATA -->
            <div class="col-md-6 form-horizontal">
                <div class="row text-center">
                    <b><?php echo Yii::t('all', 'wallet'); ?></b>
                </div>

                <div>
                    <!-- WALLET ID -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label">
                            <?php echo Yii::t('all', 'id') ?>:
                        </label>
                        <div class="col-md-6" style="margin-top: 7px;">
                            <?php echo $walletData['id'] ?>
                        </div>
                    </div>
                    <!-- END WALLET ID -->

                    <!-- WALLET NAME -->
                    <div class="row form-group">
                        <label class="col-md-3 control-label">
                            <?php echo Yii::t('all', 'name') ?>:
                        </label>
                        <div class="col-md-6" style="margin-top: 7px;">
                            <?php echo $walletData['name'] ?>
                        </div>
                    </div>
                    <!-- END WALLET NAME -->

                    <!-- WALLET PAYMENT DATA FIELDS -->
                    <?php foreach($walletData['payment_fields'] as $paymentField) { ?>
                        <div class="row form-group" ng-repeat="paymentField in walletData.payment_fields">
                            <label class="col-md-3 control-label">
                                <?php echo $paymentField['field_name'] ?>:
                            </label>
                            <div class="col-md-6" style="margin-top: 7px;">
                                <?php echo $paymentField['field_value'] ?>
                            </div>
                        </div>
                    <?php } ?>
                    <!-- END WALLET PAYMENT DATA FIELDS -->
                </div>
            </div>
            <!-- END WALLET DATA -->

            <br />

            <div class="col-md-12">
                <div class="row col-md-offset-5">
                    <button type="button"
                            class="btn btn-sm btn-default"
                            ng-click="cancelButtonClick()"
                        >
                        <?php echo Yii::t('all', 'back'); ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>