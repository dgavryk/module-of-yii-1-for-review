<?php
/**
 * @var payments\controllers\MethodsController $this
 */
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('main_menu', 'payments');
$this->breadcrumbs = [
    Yii::t('main_menu', 'payments'),
];

$this->includeAngularFile('payments/methods/list.js');
?>

<div class="row" ng-controller="PaymentsMethodsListCtrl">
    <div class="col-md-10" ng-cloak>

        <div ng-show="!isShowPaymentMethodsListLoader">
            <div class="panel panel-default">
                <div class="panel-heading" data-original-title="">
                    <h2><i class="icon-note"></i><span class="break"></span><?php echo Yii::t('main_menu', 'payments_methods') ?></h2>
                </div>
                <div class="panel-body">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline no-footer">

                        <table class="table table-striped table-bordered dataTable">
                            <thead>
                            <tr>
                                <th class="text-center"><?php echo Yii::t('all', 'method') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'currency') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'min_pay_out') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'fee') ?></th>
                                <th class="text-center"></th>
                            </tr>
                            </thead>
                            <tr ng-repeat="method in methodsList">
                                <td class="text-center"><div ng-bind="method.method_name"></div></td>
                                <td class="text-center"><div ng-bind="method.currency_name"></div></td>
                                <td class="text-center"><div ng-bind="method.min_pay_out"></div></td>
                                <td class="text-center"><div ng-bind="method.fee_description"></div></td>
                                <td class="text-center">
                                    <span class="btn btn-success"
                                          ng-click="addWalletClick(method.id)"
                                        ><?php echo Yii::t('all', 'add_wallet') ?></span>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>

        <?php $this->renderPartial('//_components/loader/mainLoader', ['id' => 'paymentMethodsListLoader']); ?>
    </div>
</div>