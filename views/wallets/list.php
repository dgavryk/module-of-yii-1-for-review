<?php
/**
 * @var payments\controllers\WalletsController $this
 */
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('main_menu', 'payments');
$this->breadcrumbs = [
    Yii::t('main_menu', 'payments'),
];

$translates = [
    'confirmDelete' => [
        'title' => \Yii::t('all', 'are_you_sure_want_to_delete_wallet'),
        'confirmButton' => \Yii::t('all', 'yes_delete_it'),
        'cancelButton' => \Yii::t('all', 'cancel'),
    ],
    'confirmActivate' => [
        'title' => \Yii::t('all', 'if_you_activate_wallet_other_active_wallet_with_same_currency_will_be_disable'),
        'confirmButton' => \Yii::t('all', 'yes_activate_it'),
        'cancelButton' => \Yii::t('all', 'cancel'),
    ],
];

$this->includeAngularFile('payments/wallets/list.js');
?>

<div class="row" ng-controller="PaymentsWalletsListCtrl">

    <div ng-init="init(
        <?php echo CHtml::encode(json_encode($translates)); ?>
    )"></div>

    <div class="col-md-10" ng-cloak>

        <div ng-show="!isShowWalletsListLoader">
            <div class="panel panel-default">
                <div class="panel-heading" data-original-title="">
                    <h2><i class="icon-note"></i><span class="break"></span><?php echo Yii::t('main_menu', 'wallets') ?></h2>
                </div>

                <!-- FILTERS -->
                <div class="row">
                    <div class="col-md-12" style="margin-top: 15px;">
                        <div class="row">
                            <!-- CURRENCY -->
                            <div class="col-sm-4">
                                <label class="col-sm-4 control-label text-right"><?= Yii::t('all', 'currency') ?>:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" ng-model="filters.currencyId" ng-change="applyFilters()">
                                        <option selected="selected" value="0"><?php echo Yii::t('all', 'all') ?></option>
                                        <?php
                                        $currencies = Currencies::getCurrenciesSimpleList();
                                        foreach($currencies as $id => $name) { ?>
                                            <option value="<?php echo $id ?>"><?php echo $name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <!-- STATUS -->
                            <div class="col-sm-4">
                                <label class="col-sm-4 control-label text-right"><?= Yii::t('all', 'status') ?>:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" ng-model="filters.status" ng-change="applyFilters()">
                                        <option selected="selected" value="0"><?php echo Yii::t('all', 'all') ?></option>
                                        <?php
                                        $statusList = \payments\models\active_records\Wallet::getStatusList(true);
                                        foreach($statusList as $id => $name) { ?>
                                            <option value="<?php echo $id ?>"><?php echo $name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FILTERS END -->

                <hr />

                <div class="panel-body">

                    <div class="row text-right">
                        <div class="col-md-offset-10 col-md-2">
                            <span class="btn btn-primary"
                                  ng-click="addWalletButtonClick()"
                                ><?php echo Yii::t('all', 'add_wallet') ?></span>
                        </div>
                    </div>

                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline no-footer">

                        <table class="table table-striped table-bordered dataTable">
                            <thead>
                            <tr>
                                <th class="text-center"><?php echo Yii::t('all', 'name') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'type') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'currency') ?></th>
                                <th class="text-center"><?php echo Yii::t('all', 'status') ?></th>
                                <th class="text-center" width="35%"><?php echo Yii::t('all', 'actions') ?></th>
                            </tr>
                            </thead>
                            <tr ng-repeat="wallet in walletsList">
                                <td class="text-center"><div ng-bind="wallet.name"></div></td>
                                <td class="text-center"><div ng-bind="wallet.payment_method_name"></div></td>
                                <td class="text-center"><div ng-bind="wallet.currency_name"></div></td>
                                <td class="text-center">
                                    <span ng-class="{'red-text': wallet.status == <?php echo \payments\models\active_records\Wallet::STATUS_NOT_ACTIVE ?>, 'green-text': wallet.status == <?php echo \payments\models\active_records\Wallet::STATUS_ACTIVE ?>}" >
                                        <div ng-bind="wallet.status_name"></div>
                                    </span>
                                </td>
                                <td class="text-center">
                                    <span class="btn btn-success"
                                          ng-show="wallet.status == <?php echo \payments\models\active_records\Wallet::STATUS_NOT_ACTIVE ?>"
                                          ng-click="activateButtonClick(wallet.id)"
                                        ><?php echo Yii::t('all', 'activate') ?></span>
                                    <span class="btn btn-primary"
                                          ng-click="editButtonClick(wallet.id)"
                                        ><?php echo Yii::t('all', 'edit') ?></span>
                                    <span class="btn btn-danger"
                                          ng-click="deleteButtonClick(wallet.id)"
                                        ><?php echo Yii::t('all', 'delete') ?></span>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>

        <?php $this->renderPartial('//_components/loader/mainLoader', ['id' => 'walletsListLoader']); ?>
    </div>
</div>