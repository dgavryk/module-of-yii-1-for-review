<?php
/**
 * @var array $field
 */
$attributeName = $field['field_attribute'];
?>

<div class="row form-group">
    <label class="col-md-3 control-label"><?php echo Yii::t('payment_method_fields', $field['field_name']) ?>:</label>
    <div class="col-md-8">
        <input type="text"
               class="form-control"
               ng-model="formData.payment_fields.<?php echo $attributeName; ?>"
               ng-class="{'error': formErrors.paymentFields.<?php echo $attributeName; ?>}"
            />
        <div class="errorMessage" ng-if="formErrors.paymentFields.<?php echo $attributeName; ?>"><div ng-bind="formErrors.paymentFields.<?php echo $attributeName; ?>[0]"></div></div>
    </div>
</div>