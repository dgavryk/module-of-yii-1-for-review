<?php
/**
 * @var payments\controllers\WalletsController $this
 * @var payments\models\active_records\Wallet $wallet
 * @var payments\models\active_records\PaymentMethod $paymentMethod
 * @var payments\models\forms\wallets\WalletFormModel $formModel
 */
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('main_menu', 'payments');
$this->breadcrumbs = [
    Yii::t('main_menu', 'payments'),
];

$this->includeAngularFile('payments/wallets/edit.js');
?>

<div class="form col-md-12" ng-controller="PaymentsWalletsEditCtrl">

    <div ng-init="init(
            <?php echo $paymentMethod->id ?>,
            <?php echo (!empty($wallet->id) ? $wallet->id : 'null' ) ?>
        )"></div>

    <div class="panel panel-default">

        <div class="panel-heading">
            <h2><strong><?php echo (!empty($wallet->id)
                        ? Yii::t('all', 'create_new_wallet')
                        : Yii::t('all', 'edit_wallet'))
                    ?></strong></h2>
        </div>

        <div class="panel-body" ng-show="!isShowProcessDataLoader" ng-cloak>
            <div class="col-md-6 form-horizontal">

                <!-- NAME -->
                <div class="row form-group">
                    <label class="col-md-3 control-label"><?php echo Yii::t('all', 'name') ?>:</label>
                    <div class="col-md-6">
                        <input type="text"
                               class="form-control"
                               ng-model="formData.name"
                               ng-class="{'error': formErrors.name}"
                            />
                        <div class="errorMessage" ng-if="formErrors.name"><div ng-bind="formErrors.name[0]"></div></div>
                    </div>
                </div>

                <!-- PAYMENT METHOD -->
                <div class="row form-group">
                    <label class="col-md-3 control-label"><?php echo Yii::t('all', 'payment_method') ?>:</label>
                    <div class="col-md-6">
                        <div style="padding-top: 7px;"><b><?php echo \payments\models\PaymentMethodsNames::getNameById($paymentMethod->payment_method_name_id) ?> (<?php echo Currencies::getCurrencyName($paymentMethod->currency_id) ?>)<b></div>
                        <input type="hidden"
                               class="form-control"
                               ng-model="formData.payment_method_id"
                               ng-class="{'error': formErrors.paymentMethodId}"
                            />
                        <div class="errorMessage" ng-if="formErrors.paymentMethodId"><div ng-bind="formErrors.paymentMethodId[0]"></div></div>
                    </div>
                </div>

                <!-- PAYMENT METHOD FIELDS -->
                <?php foreach($formModel->paymentFields as $field) { ?>
                    <?php $viewName = \payments\models\active_records\PaymentMethodField::getPaymentMethodFieldViewName($field['field_type']); ?>
                    <?php $this->renderPartial('payment_method_fields/' . $viewName, [
                        'field' => $field,
                    ]); ?>
                <?php } ?>


                <br />
                <div class="row col-md-offset-5">
                    <button type="button"
                            class="btn btn-sm btn-primary"
                            ng-click="saveDataClick()"
                        >
                        <i class="fa fa-dot-circle-o"></i> <?php echo (!empty($wallet->id) ? Yii::t('all', 'save') : Yii::t('all', 'add')); ?>
                    </button>
                    <button type="button"
                            class="btn btn-sm btn-default"
                            ng-click="cancelButtonClick()"
                        >
                        <?php echo Yii::t('all', 'back'); ?>
                    </button>
                </div>

            </div>
        </div>

        <?php $this->renderPartial('//_components/loader/mainLoader', ['id' => 'processDataLoader']); ?>

    </div>

</div>