<?php namespace payments\models\forms\invoices;

use payments\models\active_records\Invoice;
use payments\models\BalancesUpdater;
use payments\models\forms\BaseFormModel;
use payments\models\PaymentMethodsNames;

class InvoiceFormModel extends BaseFormModel
{
    public $invoiceId;
    public $userId;
    public $paymentMethodNameId;
    public $currencyId;
    public $status;
    public $payDate;
    public $payPeriodFrom;
    public $payPeriodTo;
    public $invoiceSum;
    public $feeSum;
    public $receivedSum;
    public $comment;


    /**
     * AVAILABLE SCENARIOS:
     *      create
     *      update
     *      delete
     */


    /**
     * @return array
     */
    public function rules()
    {
        return [
            // filters
            ['invoiceSum, feeSum, receivedSum, comment', 'filter', 'filter' => 'trim'],

            // validation
            [
                'invoiceId',
                'required',
                'on' => 'update, delete',
                'message' => \Yii::t('validations', 'required_field'),
            ],
            [
                'userId, currencyId, paymentMethodNameId',
                'required',
                'on' => 'create',
                'message' => \Yii::t('validations', 'required_field'),
            ],
            [
                'status, payDate, payPeriodFrom, payPeriodTo',
                'required',
                'on' => 'create, update',
                'message' => \Yii::t('validations', 'required_field'),
            ],
            [
                'status',
                'in',
                'range' => Invoice::getStatusList(false),
                'on' => 'create, update',
            ],
            [
                'userId',
                'userValidation',
                'on' => 'create, update',
            ],
            [
                'paymentMethodNameId',
                'paymentMethodNameValidation',
                'on' => 'create',
            ],
            [
                'payDate',
                'payDateValidation',
                'on' => 'create, update',
            ],
            [
                'payPeriodFrom',
                'payPeriodValidation',
                'on' => 'create, update',
            ],
            [
                'invoiceSum, feeSum, receivedSum',
                'priceSumValidation',
                'on' => 'create, update',
            ],
            [
                'comment',
                'length',
                'min' => 1, 'max' => 250,
            ],

            //['', 'unsafe'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     */
    public function priceSumValidation($attribute, $params)
    {
        if (!preg_match('/^[0-9]{1,9}(\.[0-9]{1,2})?$/', $this->{$attribute}) || $this->{$attribute} < 0) {
            $this->addError($attribute, \Yii::t('validations', 'wrong_format_must_be_0.00'));
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     */
    public function userValidation($attribute, $params)
    {
        if (!empty($this->userId)) {
            $user = \User::model()->findByPk($this->userId);
            if (empty($user) || $user->role_id != \User::ROLE_BUYER) {
                $this->addError('userId', \Yii::t('validations', 'value_is_wrong'));
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     */
    public function paymentMethodNameValidation($attribute, $params)
    {
        if (!empty($this->paymentMethodNameId)) {
            $name = PaymentMethodsNames::getNameById($this->paymentMethodNameId);
            if (is_null($name)) {
                $this->addError('walletId', \Yii::t('paymentMethodNameId', 'value_is_wrong'));
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     */
    public function payDateValidation($attribute, $params)
    {
        if (!empty($this->payDate)) {
            if (!preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/', $this->payDate) || !strtotime($this->payDate)) {
                $this->addError('payDate', \Yii::t('validations', 'wrong_date_format'));
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     */
    public function payPeriodValidation($attribute, $params)
    {
        if (!empty($this->payPeriodTo) && !empty($this->payPeriodTo)) {
            if (!preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/', $this->payPeriodFrom) || !strtotime($this->payPeriodFrom)) {
                $this->addError('payPeriodFrom', \Yii::t('validations', 'wrong_date_format'));
                $this->addError('payPeriod', \Yii::t('validations', 'wrong_date_format'));
            } elseif (!preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/', $this->payPeriodTo) || !strtotime($this->payPeriodTo)) {
                $this->addError('payPeriodTo', \Yii::t('validations', 'wrong_date_format'));
                $this->addError('payPeriod', \Yii::t('validations', 'wrong_date_format'));
            } elseif (strtotime($this->payPeriodFrom) > strtotime($this->payPeriodTo)) {
                $this->addError('payPeriodFrom', \Yii::t('validations', 'time_from_can_not_be_bigger_than_time_to'));
                $this->addError('payPeriod', \Yii::t('validations', 'time_from_can_not_be_bigger_than_time_to'));
            }
        } else {
            $this->addError('payPeriod', \Yii::t('validations', 'required_field'));
        }
    }

    /**
     * Get buyer name
     *
     * @return string|null
     */
    public function getBuyerName()
    {
        $name = null;
        if (!empty($this->userId)) {
            $user = \User::model()->findByPk($this->userId);
            if ($user) {
                $name = $user->username;
            }
        }
        return $name;
    }

    /**
     * Get currency name
     *
     * @return string|null
     */
    public function getCurrencyName()
    {
        $name = null;
        if (!empty($this->currencyId)) {
            $currencyName = \Currencies::getCurrencyName($this->currencyId);
            if ($currencyName) {
                $name = $currencyName;
            }
        }
        return $name;
    }

    /**
     * Get payment method name
     *
     * @return string|null
     */
    public function getPaymentMethodName()
    {
        $name = null;
        if (!empty($this->paymentMethodNameId)) {
            $paymentMethodName = PaymentMethodsNames::getNameById($this->paymentMethodNameId);
            if ($paymentMethodName) {
                $name = $paymentMethodName;
            }
        }
        return $name;
    }

    /**
     * Set data for form
     *
     * @param array $setData
     * @param int|null $invoiceId
     */
    public function setData($setData, $invoiceId = null)
    {
        !empty($invoiceId)  && $this->invoiceId = $invoiceId;

        !empty($setData['userId'])          && $this->userId = $setData['userId'];
        !empty($setData['paymentMethodNameId']) && $this->paymentMethodNameId = $setData['paymentMethodNameId'];
        !empty($setData['currencyId'])      && $this->currencyId = $setData['currencyId'];
        !empty($setData['status'])          && $this->status = $setData['status'];
        !empty($setData['payDate'])         && $this->payDate = $setData['payDate'];
        !empty($setData['payPeriodFrom'])   && $this->payPeriodFrom = $setData['payPeriodFrom'];
        !empty($setData['payPeriodTo'])     && $this->payPeriodTo = $setData['payPeriodTo'];
        !empty($setData['invoiceSum'])      && $this->invoiceSum = $setData['invoiceSum'];
        !empty($setData['feeSum'])          && $this->feeSum = $setData['feeSum'];
        !empty($setData['receivedSum'])     && $this->receivedSum = $setData['receivedSum'];
        !empty($setData['comment'])         && $this->comment = $setData['comment'];
    }

    /**
     * Get data for view
     *
     * @return array
     */
    public function getData()
    {
        $data = [
            'invoiceId'             => $this->invoiceId,
            'userId'                => $this->userId,
            'paymentMethodNameId'   => $this->paymentMethodNameId,
            'status'                => $this->status,
            'currencyId'            => $this->currencyId,
            'payDate'               => $this->payDate,
            'payPeriodFrom'         => $this->payPeriodFrom,
            'payPeriodTo'           => $this->payPeriodTo,
            'invoiceSum'            => round($this->invoiceSum, 2),
            'feeSum'                => round($this->feeSum, 2),
            'receivedSum'           => round($this->receivedSum, 2),
            'comment'               => $this->comment,
            // additional fields
            'userName'      => $this->getBuyerName(),
            'currencyName'  => $this->getCurrencyName(),
        ];

        return $data;
    }

    /**
     * Load form data for invoice
     *
     * @param \payments\models\active_records\Invoice $invoice
     */
    public function loadData($invoice)
    {
        $setData = [
            'userId'        => $invoice->user_id,
            'paymentMethodNameId' => $invoice->payment_method_name_id,
            'currencyId'    => $invoice->currency_id,
            'status'        => $invoice->status,
            'payDate'       => $invoice->pay_date,
            'payPeriodFrom' => $invoice->pay_period_from,
            'payPeriodTo'   => $invoice->pay_period_to,
            'invoiceSum'    => isset($invoice->invoice_sum) ? round($invoice->invoice_sum, 2) : null,
            'feeSum'        => isset($invoice->fee_sum) ? round($invoice->fee_sum, 2) : null,
            'receivedSum'   => isset($invoice->received_sum) ? round($invoice->received_sum, 2) : null,
            'comment'       => $invoice->comment,
        ];

        $this->setData($setData, $invoice->id);
    }


    /**
     * @return bool
     * @throws \CDbException
     */
    public function saveData()
    {
        $allSaved = false;
        $transaction = \Yii::app()->db->beginTransaction();
        try {
            $allSaved = $this->_saveInvoice();
            $allSaved ? $transaction->commit() : $transaction->rollback();
        } catch(\Exception $e) {
            $transaction->rollback();
        }
        return $allSaved;
    }

    /**
     * Save invoice
     *
     * @return bool|int
     */
    protected function _saveInvoice()
    {
        if (!empty($this->invoiceId)) {
            $invoice = Invoice::model()->findByPk($this->invoiceId);
        } else {
            $invoice = new Invoice();
            $invoice->creation_datetime = date('Y-m-d H:i:s');
            $invoice->user_id           = $this->userId;
            $invoice->payment_method_name_id = $this->paymentMethodNameId;
            $invoice->currency_id       = $this->currencyId;
            $invoice->is_removed        = 0;
        }

        if (empty($invoice)) {
            $this->addError('invoiceId', \Yii::t('validations', 'invoice_validation_is_false'));
            return false;
        }

        $needUpdateBalance = false;
        if ($this->status == Invoice::STATUS_PAID && $this->status != $invoice->status) {
            $needUpdateBalance = true;
        }

        isset($this->status)        && $invoice->status = $this->status;
        isset($this->payDate)       && $invoice->pay_date = $this->payDate;
        isset($this->payPeriodFrom) && $invoice->pay_period_from = $this->payPeriodFrom;
        isset($this->payPeriodTo)   && $invoice->pay_period_to = $this->payPeriodTo;
        isset($this->invoiceSum)    && $invoice->invoice_sum = $this->invoiceSum;
        isset($this->feeSum)        && $invoice->fee_sum = $this->feeSum;
        isset($this->receivedSum)   && $invoice->received_sum = $this->receivedSum;
        isset($this->comment)       && $invoice->comment = $this->comment;

        if (!$invoice->save()) {
            $this->addError('invoiceId', \Yii::t('validations', 'invoice_validation_is_false'));
            return false;
        }

        $this->invoiceId = $invoice->id;

        // update balances
        if ($needUpdateBalance) {
            $balancesUpdater = new BalancesUpdater();
            $isBalanceUpdate = $balancesUpdater->updateBalanceOnInvoice($invoice->user_id, $invoice->invoice_sum, $invoice->currency_id, $invoice->id);
            if (!$isBalanceUpdate) {
                $this->addError('invoiceId', \Yii::t('validations', 'update_balance_error'));
                return false;
            }
        }

        return true;
    }


    /**
     * @return bool
     * @throws \CDbException
     */
    public function deleteData()
    {
        $allDeleted = false;
        $transaction = \Yii::app()->db->beginTransaction();
        try {
            $allDeleted = $this->_deleteInvoice();
            $allDeleted ? $transaction->commit() : $transaction->rollback();
        } catch(\Exception $e) {
            $transaction->rollback();
        }
        return $allDeleted;
    }

    /**
     * Delete invoice
     *
     * @return bool
     */
    protected function _deleteInvoice()
    {
        $isDeleted = false;

        $invoice = Invoice::model()->findByPk($this->invoiceId); /** @var \payments\models\active_records\Invoice $invoice */
        if (!$invoice) {
            $this->addError('invoiceId', \Yii::t('validations', 'invoice_is_wrong'));
            return $isDeleted;
        }

        $invoice->is_removed = 1;

        if ($invoice->save()) {
            $isDeleted = true;
        } else {
            $this->addError('invoiceId', \Yii::t('validations', 'invoice_validation_is_false'));
        }

        // update balances
        if ($isDeleted && $invoice->status == Invoice::STATUS_PAID) {
            $balancesUpdater = new BalancesUpdater();
            $isBalanceUpdate = $balancesUpdater->updateBalanceOnInvoiceDelete($invoice->user_id, $invoice->invoice_sum, $invoice->currency_id, $invoice->id);
            if (!$isBalanceUpdate) {
                $isDeleted = false;
                $this->addError('invoiceId', \Yii::t('validations', 'update_balance_error'));
                return $isDeleted;
            }
        }

        return $isDeleted;
    }
}