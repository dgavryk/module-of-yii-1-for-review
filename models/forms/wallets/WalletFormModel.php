<?php namespace payments\models\forms\wallets;

use payments\models\active_records\PaymentMethod;
use payments\models\active_records\PaymentMethodField;
use payments\models\active_records\Wallet;
use payments\models\active_records\WalletPaymentField;
use payments\models\forms\BaseFormModel;
use payments\models\PaymentsMethods;
use payments\models\PaymentsMethodsFields;
use payments\models\Wallets;
use payments\models\WalletsPaymentsFields;

class WalletFormModel extends BaseFormModel
{
    public $walletId;
    public $paymentMethodId;
    public $name;
    public $status;
    public $paymentFields = [];

    protected $_paymentFieldsErrors = [];


    /**
     * AVAILABLE SCENARIOS:
     *      create
     *      update
     *      updateStatus
     *      delete
     */


    /**
     * @return array
     */
    public function rules()
    {
        return [
            // filters
            ['name', 'filter', 'filter' => 'trim'],

            // validation
            [
                'walletId',
                'required',
                'on' => 'update',
                'message' => \Yii::t('validations', 'required_field'),
            ],
            [
                'paymentMethodId, name',
                'required',
                'on' => 'create, update',
                'message' => \Yii::t('validations', 'required_field'),
            ],
            [
                'status',
                'in',
                'range' => Wallet::getStatusList(false),
                'on' => 'updateStatus',
            ],
            [
                'status',
                'required',
                'on' => 'updateStatus',
                'message' => \Yii::t('validations', 'required_field'),
            ],
            [
                'paymentFields',
                'paymentFieldsValidation',
                'on' => 'create, update',
            ],

            //['', 'unsafe'],
        ];
    }

    /**
     * Get validation errors
     *
     * @param string|null $attribute
     * @return array
     */
    public function getErrors($attribute = null)
    {
        $errors = parent::getErrors($attribute);

        if (!empty($attribute)) {
            if ($attribute == 'paymentFields') {
                $errors = $this->_paymentFieldsErrors;
            }
        } else {
            if (!empty($this->_paymentFieldsErrors)) {
                $errors['paymentFields'] = $this->_paymentFieldsErrors;
            }
        }

        return $errors;
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     */
    public function paymentFieldsValidation($attribute, $params)
    {
        foreach($this->paymentFields as $attributeName => $field) {
            if (!empty($field['value'])) {
                if ($field['field_type'] == PaymentMethodField::FIELD_TYPE_DEFAULT_STRING) {

                    $this->paymentFields[$attributeName]['value'] = trim($field['value']);
                    $field['value'] = trim($field['value']);
                    if (mb_strlen($field['value']) <= 0 || mb_strlen($field['value']) > 50) {
                        $this->_paymentFieldsErrors[$attributeName][] = \Yii::t('validations', 'text_length_must_be_from_{min}_to_{max}', ['{min}' => 1, '{max}' => 50]);
                    }

                } else {
                    throw new \Exception('Wrong payment method field type on validation');
                }
            } elseif (!empty($field['extra_params']) && !empty($field['extra_params']['is_required'])) {
                $this->_paymentFieldsErrors[$attributeName][] = \Yii::t('validations', 'required_field');
            }
        }

        if (!empty($this->_paymentFieldsErrors)) {
            $this->addError('paymentFields', 'wallet_validation_is_false'); // then it rewrite on array
        }
    }


    /**
     * @return array
     */
    public function getTranslates()
    {
        $translates = [
            'confirmDeleteBanner' => [
                'title' => \Yii::t('all', 'are_you_really_want_to_delete_banner'),
                'confirmButton' => \Yii::t('all', 'yes_delete_it'),
                'cancelButton' => \Yii::t('all', 'cancel'),
            ],
        ];

        return $translates;
    }

    /**
     * Set data for form
     *
     * @param array $setData
     * @param int|null $walletId
     */
    public function setData($setData, $walletId = null)
    {
        !empty($walletId)               && $this->walletId = $walletId;

        !empty($setData['payment_method_id'])   && $this->paymentMethodId = $setData['payment_method_id'];
        !empty($setData['name'])                && $this->name = $setData['name'];
        !empty($setData['status'])              && $this->status = $setData['status'];

        $this->loadPaymentFields();
        if (!empty($setData['payment_fields'])) {
            $this->setPaymentFieldData($setData['payment_fields']);
        }
    }

    /**
     * Get data for view
     *
     * @return array
     */
    public function getData()
    {
        $data = [
            'name' => $this->name,
            'payment_method_id' => $this->paymentMethodId,
            'status' => $this->status,
            'payment_fields' => [],
        ];

        foreach($this->paymentFields as $attributeName => $field) {
            if(isset($field['value'])) {
                $data['payment_fields'][$attributeName] = $field['value'];
            }
        }

        return $data;
    }

    /**
     * Load form data for wallet
     *
     * @param \payments\models\active_records\Wallet $wallet
     */
    public function loadData($wallet)
    {
        $setData = [
            'name'              => $wallet->name,
            'payment_method_id' => $wallet->payment_method_id,
            'status'            => $wallet->status,
        ];

        $walletsPaymentsFields = new WalletsPaymentsFields();
        $paymentFieldsValues = $walletsPaymentsFields->getWalletPaymentFieldsValues($wallet->id, true);
        if (!empty($paymentFieldsValues)) {
            $setData['payment_fields'] = $paymentFieldsValues;
        }

        $this->setData($setData, $wallet->id);
    }

    /**
     * Load payment fields
     */
    public function loadPaymentFields()
    {
        $paymentsMethodsFields = new PaymentsMethodsFields();
        $fields = $paymentsMethodsFields->getFieldsForPaymentMethod($this->paymentMethodId);
        foreach($fields as $v) {
            $this->paymentFields[$v['field_attribute']] = $v;
        }
    }

    /**
     * Set data for payment fields
     *
     * @param array $paymentData
     */
    protected function setPaymentFieldData($paymentData)
    {
        if (!empty($paymentData)) {
            foreach($this->paymentFields as $fieldAttribute => $v) {
                if (isset($paymentData[$fieldAttribute])) {
                    if (!empty($paymentData[$fieldAttribute]) || strval($paymentData[$fieldAttribute]) === '0') {
                        $this->paymentFields[$fieldAttribute]['value'] = $paymentData[$fieldAttribute];
                    }
                }
            }
        }
    }

    /**
     * @return bool
     * @throws \CDbException
     */
    public function saveData()
    {
        $allSaved = false;
        $transaction = \Yii::app()->db->beginTransaction();
        try {
            $allSaved = $this->_saveWallet();
            $allSaved && $allSaved = $this->_saveWalletPaymentFields();
            $allSaved ? $transaction->commit() : $transaction->rollback();
        } catch(\Exception $e) {
            $transaction->rollback();
        }
        return $allSaved;
    }

    /**
     * Save wallet
     *
     * @return bool|int
     */
    protected function _saveWallet()
    {
        if (!empty($this->walletId)) {
            $wallet = Wallet::model()->findByPk($this->walletId);
        } else {
            $wallet = new Wallet();
            $wallet->user_id            = \UsersPermissions::getCurrentUserId();
            $wallet->payment_method_id  = $this->paymentMethodId;
            $wallet->is_removed         = 0;
            $wallet->status             = Wallet::STATUS_NOT_ACTIVE;
        }

        if (empty($wallet)) {
            $this->addError('walletId', \Yii::t('validations', 'wallet_validation_is_false'));
            return false;
        }

        $wallet->name = $this->name;

        if (!$wallet->save()) {
            $this->addError('walletId', \Yii::t('validations', 'wallet_validation_is_false'));
            return false;
        }

        $this->walletId = $wallet->id;

        return true;
    }

    /**
     * Save wallet payment field
     *
     * @return bool
     * @throws \CDbException
     */
    protected function _saveWalletPaymentFields()
    {
        if (!empty($this->walletId) && !empty($this->paymentFields)) {
            \Yii::app()->db->createCommand()->delete(
                WalletPaymentField::model()->tableName(),
                'wallet_id = :wallet_id',
                [':wallet_id' => $this->walletId]
            );

            $insertData = [];
            foreach($this->paymentFields as $field) {
                if (isset($field['value'])) {
                    $insertData[] = [
                        'wallet_id' => $this->walletId,
                        'payment_method_field_id' => $field['id'],
                        'field_value' => $field['value'],
                    ];
                }
            }

            if (!empty($insertData)) {
                \Yii::app()->db->schema->commandBuilder->createMultipleInsertCommand(
                    WalletPaymentField::model()->tableName(),
                    $insertData
                )->execute();
            }
        }
        return true;
    }


    /**
     * @return bool
     * @throws \CDbException
     */
    public function deleteData()
    {
        $allDeleted = false;
        $transaction = \Yii::app()->db->beginTransaction();
        try {
            $allDeleted = $this->_deleteWallet();
            $allDeleted ? $transaction->commit() : $transaction->rollback();
        } catch(\Exception $e) {
            $transaction->rollback();
        }
        return $allDeleted;
    }

    /**
     * Delete banner
     *
     * @return bool
     */
    protected function _deleteWallet()
    {
        $isDeleted = false;

        $wallet = Wallet::model()->findByPk($this->walletId); /** @var \payments\models\active_records\Wallet $wallet */
        if (!$wallet) {
            $this->addError('walletId', \Yii::t('validations', 'wallet_is_wrong'));
            return $isDeleted;
        }

        $wallet->is_removed = 1;

        if ($wallet->save()) {
            $isDeleted = true;
        } else {
            $this->addError('walletId', \Yii::t('validations', 'wallet_validation_is_false'));
        }

        return $isDeleted;
    }

    /**
     * @return bool
     * @throws \CDbException
     */
    public function updateStatusData()
    {
        $updated = false;
        $transaction = \Yii::app()->db->beginTransaction();
        try {
            $updated = $this->_updateStatus();
            $updated ? $transaction->commit() : $transaction->rollback();
        } catch(\Exception $e) {
            $transaction->rollback();
        }
        return $updated;
    }

    /**
     * Update wallet status
     *
     * @return bool
     */
    protected function _updateStatus()
    {
        $isUpdated = false;

        $wallet = Wallet::model()->findByPk($this->walletId); /** @var \payments\models\active_records\Wallet $wallet */
        if (!$wallet) {
            $this->addError('walletId', \Yii::t('validations', 'wallet_is_wrong'));
            return $isUpdated;
        }

        // if new status is active, than we need disable another wallets with same currency
        if ($this->status == Wallet::STATUS_ACTIVE) {
            $paymentsMethods = new PaymentsMethods();
            $paymentMethod = $paymentsMethods->getPaymentMethodById($wallet->payment_method_id, true, true);

            if (!empty($paymentMethod)) {
                $walletIds = \Yii::app()->db->createCommand()
                    ->select('w.id')
                    ->from(Wallet::model()->tableName() . ' AS w')
                    ->leftJoin(PaymentMethod::model()->tableName() . ' AS pm', 'pm.id = w.payment_method_id')
                    ->where('pm.currency_id = :currency_id', [':currency_id' => $paymentMethod->currency_id])
                    ->queryColumn();

                if (!empty($walletIds)) { // disable all wallets in same currency
                    \Yii::app()->db->createCommand()->update(
                        Wallet::model()->tableName(),
                        ['status' => Wallet::STATUS_NOT_ACTIVE],
                        'id IN (' . implode(',', $walletIds) . ')'
                    );
                }
            } else {
                $this->addError('walletId', \Yii::t('validations', 'wallet_is_wrong'));
                return $isUpdated;
            }
        }

        $wallet->status = $this->status;

        if ($wallet->save()) {
            $isUpdated = true;
        }

        return $isUpdated;
    }
}