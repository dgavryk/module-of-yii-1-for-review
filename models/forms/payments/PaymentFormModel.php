<?php namespace payments\models\forms\payments;

use payments\models\active_records\Payment;
use payments\models\active_records\Wallet;
use payments\models\BalancesUpdater;
use payments\models\forms\BaseFormModel;
use payments\models\Wallets;

class PaymentFormModel extends BaseFormModel
{
    public $paymentId;
    public $userId;
    public $walletId;
    public $status;
    public $payDate;
    public $payPeriodFrom;
    public $payPeriodTo;
    public $paidSum;
    public $feeSum;
    public $receivedSum;
    public $comment;


    /**
     * AVAILABLE SCENARIOS:
     *      create
     *      update
     *      delete
     */


    /**
     * @return array
     */
    public function rules()
    {
        return [
            // filters
            ['paidSum, feeSum, receivedSum, comment', 'filter', 'filter' => 'trim'],

            // validation
            [
                'paymentId',
                'required',
                'on' => 'update, delete',
                'message' => \Yii::t('validations', 'required_field'),
            ],
            [
                'userId, walletId',
                'required',
                'on' => 'create',
                'message' => \Yii::t('validations', 'required_field'),
            ],
            [
                'status, payDate, payPeriodFrom, payPeriodTo',
                'required',
                'on' => 'create, update',
                'message' => \Yii::t('validations', 'required_field'),
            ],
            [
                'status',
                'in',
                'range' => Payment::getStatusList(false),
                'on' => 'create, update',
            ],
            [
                'userId',
                'userValidation',
                'on' => 'create, update',
            ],
            [
                'walletId',
                'walletValidation',
                'on' => 'create, update',
            ],
            [
                'payDate',
                'payDateValidation',
                'on' => 'create, update',
            ],
            [
                'payPeriodFrom',
                'payPeriodValidation',
                'on' => 'create, update',
            ],
            [
                'paidSum, feeSum, receivedSum',
                'priceSumValidation',
                'on' => 'create, update',
            ],
            [
                'comment',
                'length',
                'min' => 1, 'max' => 250,
            ],

            //['', 'unsafe'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     */
    public function priceSumValidation($attribute, $params)
    {
        if (!preg_match('/^[0-9]{1,9}(\.[0-9]{1,2})?$/', $this->{$attribute}) || $this->{$attribute} <= 0) {
            $this->addError($attribute, \Yii::t('validations', 'wrong_format_must_be_0.00'));
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     */
    public function userValidation($attribute, $params)
    {
        if (!empty($this->userId)) {
            $user = \User::model()->findByPk($this->userId);
            if (empty($user) || $user->role_id != \User::ROLE_SELLER) {
                $this->addError('userId', \Yii::t('validations', 'value_is_wrong'));
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     */
    public function walletValidation($attribute, $params)
    {
        if (!empty($this->walletId)) {
            $wallets = new Wallets();
            $isCheckAccess = (\UsersPermissions::getCurrentUserRoleId(true) != \User::ROLE_ADMIN);
            $wallet = $wallets->getWalletById($this->walletId, true, $isCheckAccess, true);
            if (empty($wallet) || $wallet->status != Wallet::STATUS_ACTIVE) {
                $this->addError('walletId', \Yii::t('validations', 'value_is_wrong'));
            } elseif (!empty($this->userId) && $this->userId != $wallet->user_id) {
                $this->addError('userId', \Yii::t('validations', 'value_is_wrong'));
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     */
    public function payDateValidation($attribute, $params)
    {
        if (!empty($this->payDate)) {
            if (!preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/', $this->payDate) || !strtotime($this->payDate)) {
                $this->addError('payDate', \Yii::t('validations', 'wrong_date_format'));
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \Exception
     */
    public function payPeriodValidation($attribute, $params)
    {
        if (!empty($this->payPeriodTo) && !empty($this->payPeriodTo)) {
            if (!preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/', $this->payPeriodFrom) || !strtotime($this->payPeriodFrom)) {
                $this->addError('payPeriodFrom', \Yii::t('validations', 'wrong_date_format'));
                $this->addError('payPeriod', \Yii::t('validations', 'wrong_date_format'));
            } elseif (!preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/', $this->payPeriodTo) || !strtotime($this->payPeriodTo)) {
                $this->addError('payPeriodTo', \Yii::t('validations', 'wrong_date_format'));
                $this->addError('payPeriod', \Yii::t('validations', 'wrong_date_format'));
            } elseif (strtotime($this->payPeriodFrom) > strtotime($this->payPeriodTo)) {
                $this->addError('payPeriodFrom', \Yii::t('validations', 'time_from_can_not_be_bigger_than_time_to'));
                $this->addError('payPeriod', \Yii::t('validations', 'time_from_can_not_be_bigger_than_time_to'));
            }
        } else {
            $this->addError('payPeriod', \Yii::t('validations', 'required_field'));
        }
    }

    /**
     * Get seller name
     *
     * @return string|null
     */
    public function getSellerName()
    {
        $name = null;
        if (!empty($this->userId)) {
            $user = \User::model()->findByPk($this->userId);
            if ($user) {
                $name = $user->username;
            }
        }
        return $name;
    }

    /**
     * Get currency name
     *
     * @return string|null
     */
    public function getCurrencyName()
    {
        $name = null;
        if (!empty($this->walletId)) {
            $wallets = new Wallets();
            $wallet = $wallets->getWalletData($this->walletId);
            if (!empty($wallet) && !empty($wallet['currency_name'])) {
                $name = $wallet['currency_name'];
            }
        }
        return $name;
    }

    /**
     * Set data for form
     *
     * @param array $setData
     * @param int|null $paymentId
     */
    public function setData($setData, $paymentId = null)
    {
        !empty($paymentId)  && $this->paymentId = $paymentId;

        !empty($setData['userId'])          && $this->userId = $setData['userId'];
        !empty($setData['walletId'])        && $this->walletId = $setData['walletId'];
        !empty($setData['status'])          && $this->status = $setData['status'];
        !empty($setData['payDate'])         && $this->payDate = $setData['payDate'];
        !empty($setData['payPeriodFrom'])   && $this->payPeriodFrom = $setData['payPeriodFrom'];
        !empty($setData['payPeriodTo'])     && $this->payPeriodTo = $setData['payPeriodTo'];
        !empty($setData['paidSum'])         && $this->paidSum = $setData['paidSum'];
        !empty($setData['feeSum'])          && $this->feeSum = $setData['feeSum'];
        !empty($setData['receivedSum'])     && $this->receivedSum = $setData['receivedSum'];
        !empty($setData['comment'])         && $this->comment = $setData['comment'];
    }

    /**
     * Get data for view
     *
     * @return array
     */
    public function getData()
    {
        $data = [
            'paymentId'     => $this->paymentId,
            'userId'        => $this->userId,
            'walletId'      => $this->walletId,
            'status'        => $this->status,
            'payDate'       => $this->payDate,
            'payPeriodFrom' => $this->payPeriodFrom,
            'payPeriodTo'   => $this->payPeriodTo,
            'paidSum'       => round($this->paidSum, 2),
            'feeSum'        => round($this->feeSum, 2),
            'receivedSum'   => round($this->receivedSum, 2),
            'comment'       => $this->comment,
            // additional fields
            'userName'      => $this->getSellerName(),
            'currencyName'  => $this->getCurrencyName(),
        ];

        return $data;
    }

    /**
     * Load form data for payment
     *
     * @param \payments\models\active_records\Payment $payment
     */
    public function loadData($payment)
    {
        $setData = [
            'userId'        => $payment->user_id,
            'walletId'      => $payment->wallet_id,
            'status'        => $payment->status,
            'payDate'       => $payment->pay_date,
            'payPeriodFrom' => $payment->pay_period_from,
            'payPeriodTo'   => $payment->pay_period_to,
            'paidSum'       => isset($payment->paid_sum) ? $payment->paid_sum : null,
            'feeSum'        => isset($payment->fee_sum) ? round($payment->fee_sum, 2) : null,
            'receivedSum'   => isset($payment->received_sum) ? $payment->received_sum : null,
            'comment'       => $payment->comment,
        ];

        $this->setData($setData, $payment->id);
    }


    /**
     * @return bool
     * @throws \CDbException
     */
    public function saveData()
    {
        $allSaved = false;
        $transaction = \Yii::app()->db->beginTransaction();
        try {
            $allSaved = $this->_savePayment();
            $allSaved ? $transaction->commit() : $transaction->rollback();
        } catch(\Exception $e) {
            $transaction->rollback();
        }
        return $allSaved;
    }

    /**
     * Save payment
     *
     * @return bool|int
     */
    protected function _savePayment()
    {
        if (!empty($this->paymentId)) {
            $payment = Payment::model()->findByPk($this->paymentId);
        } else {
            $payment = new Payment();
            $payment->creation_datetime = date('Y-m-d H:i:s');
            $payment->user_id           = $this->userId;
            $payment->wallet_id         = $this->walletId;
            $payment->is_removed        = 0;
        }

        if (empty($payment)) {
            $this->addError('paymentId', \Yii::t('validations', 'payment_validation_is_false'));
            return false;
        }

        $wallets = new Wallets();
        $wallet = $wallets->getWalletData($payment->wallet_id);
        if (empty($wallet) && !empty($wallet['currency_id'])) {
            $this->addError('walletId', \Yii::t('validations', 'value_is_wrong'));
            return false;
        }

        $needUpdateBalance = false;
        if ($this->status == Payment::STATUS_PAID && $this->status != $payment->status) {
            $needUpdateBalance = true;
        }

        isset($this->status)        && $payment->status = $this->status;
        isset($this->payDate)       && $payment->pay_date = $this->payDate;
        isset($this->payPeriodFrom) && $payment->pay_period_from = $this->payPeriodFrom;
        isset($this->payPeriodTo)   && $payment->pay_period_to = $this->payPeriodTo;
        isset($this->paidSum)       && $payment->paid_sum = $this->paidSum;
        isset($this->feeSum)        && $payment->fee_sum = $this->feeSum;
        isset($this->receivedSum)   && $payment->received_sum = $this->receivedSum;
        isset($this->comment)       && $payment->comment = $this->comment;

        if (!$payment->save()) {
            $this->addError('paymentId', \Yii::t('validations', 'payment_validation_is_false'));
            return false;
        }

        $this->paymentId = $payment->id;

        // update balances
        if ($needUpdateBalance) {
            $balancesUpdater = new BalancesUpdater();
            $isBalanceUpdate = $balancesUpdater->updateBalanceOnPayment($payment->user_id, $payment->paid_sum, $wallet['currency_id'], $payment->id);
            if (!$isBalanceUpdate) {
                $this->addError('paymentId', \Yii::t('validations', 'update_balance_error'));
                return false;
            }
        }

        return true;
    }


    /**
     * @return bool
     * @throws \CDbException
     */
    public function deleteData()
    {
        $allDeleted = false;
        $transaction = \Yii::app()->db->beginTransaction();
        try {
            $allDeleted = $this->_deletePayment();
            $allDeleted ? $transaction->commit() : $transaction->rollback();
        } catch(\Exception $e) {
            $transaction->rollback();
        }
        return $allDeleted;
    }

    /**
     * Delete payment
     *
     * @return bool
     */
    protected function _deletePayment()
    {
        $isDeleted = false;

        $payment = Payment::model()->findByPk($this->paymentId); /** @var \payments\models\active_records\Payment $payment */
        if (!$payment) {
            $this->addError('paymentId', \Yii::t('validations', 'payment_is_wrong'));
            return $isDeleted;
        }

        $wallets = new Wallets();
        $wallet = $wallets->getWalletData($payment->wallet_id);
        if (empty($wallet) && !empty($wallet['currency_id'])) {
            $this->addError('walletId', \Yii::t('validations', 'value_is_wrong'));
            return $isDeleted;
        }

        $payment->is_removed = 1;

        if ($payment->save()) {
            $isDeleted = true;
        } else {
            $this->addError('walletId', \Yii::t('validations', 'payment_validation_is_false'));
        }

        // update balances
        if ($isDeleted && $payment->status == Payment::STATUS_PAID) {
            $balancesUpdater = new BalancesUpdater();
            $isBalanceUpdate = $balancesUpdater->updateBalanceOnPaymentDelete($payment->user_id, $payment->paid_sum, $wallet['currency_id'], $payment->id);
            if (!$isBalanceUpdate) {
                $isDeleted = false;
                $this->addError('paymentId', \Yii::t('validations', 'update_balance_error'));
                return $isDeleted;
            }
        }

        return $isDeleted;
    }
}