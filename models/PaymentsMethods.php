<?php namespace payments\models;

use payments\models\active_records\PaymentMethod;
use payments\models\active_records\PaymentMethodName;

class PaymentsMethods
{

    protected static $_cache = []; // memorization


    /**
     * Get payment method by id
     *
     * @param int $id
     * @param bool $isObject
     * @param bool $checkOnRemoved
     * @return array|\payments\models\active_records\PaymentMethod|null
     */
    public function getPaymentMethodById($id, $isObject = true, $checkOnRemoved = true)
    {
        $cacheKey = __CLASS__ . ';' . __METHOD__ . ';' . implode(';', [$id, $isObject, $checkOnRemoved]);
        if (!array_key_exists($cacheKey, self::$_cache)) {
            $result = null;
            if (!empty($id)) {
                if ($isObject) {
                    $whereParams = ['id' => $id];
                    $checkOnRemoved && $whereParams['is_removed'] = 0;
                    $paymentMethod = PaymentMethod::model()->findByAttributes($whereParams); /** @var PaymentMethod $paymentMethod */
                    if ($paymentMethod) {
                        $result = $paymentMethod;
                    }
                } else {
                    $command = \Yii::app()->db->createCommand()
                        ->select('*')
                        ->from(PaymentMethod::model()->tableName())
                        ->where('id = :id', [':id' => $id]);
                    if ($checkOnRemoved) {
                        $command->andWhere('is_removed = 0');
                    }
                    $result = $command->queryRow();
                }
            }
            self::$_cache[$cacheKey] = !empty($result) ? $result : null;
        }

        return self::$_cache[$cacheKey];
    }

    /**
     * Get list of all payment methods
     *
     * @return array
     */
    public function getAllPaymentMethods()
    {
        $result = \Yii::app()->db->createCommand()
            ->select('pm.*, pmn.name AS method_name, c.code AS currency_name')
            ->from(PaymentMethod::model()->tableName() . ' AS pm')
            ->leftJoin(\Currency::model()->tableName() . ' AS c', 'c.id = pm.currency_id')
            ->leftJoin(PaymentMethodName::model()->tableName() . ' AS pmn', 'pmn.id = pm.payment_method_name_id')
            ->where('pm.is_removed = 0')
            ->order('pmn.name')
            ->queryAll();

        return $result;
    }
}