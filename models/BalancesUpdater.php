<?php namespace payments\models;

use components\LogStack;
use payments\models\active_records\Balance;
use payments\models\active_records\logs\LogBalanceChange;

class BalancesUpdater
{


    /**
     * Update balance at users on lead sold
     *
     * !!! WARNING !!! METHOD MUST BE CALL ONLY INSIDE DB TRANSACTION!!!
     * !!! WARNING !!! Logs will save only if outer transaction will be commit
     *
     * @param \Ping $ping
     * @param array $transit
     * @param array $soldData
     * @return bool
     */
    public function updateBalanceOnLeadSold($ping, $transit, $soldData)
    {
        $isUpdated = false;

        $balances = new Balances();

        $buyerBalance   = $balances->getBalanceForUser($transit['buyer_id'], $ping->monetization_currency_id, true, true); /** @var \payments\models\active_records\Balance $sellerBalance */
        $sellerBalance  = $balances->getBalanceForUser($transit['seller_id'], $ping->monetization_currency_id, true, true); /** @var \payments\models\active_records\Balance $sellerBalance */

        if (!empty($buyerBalance) && !empty($sellerBalance)) {
            $isUpdated = $this->_updateBuyerBalanceOnLeadSold($buyerBalance, $soldData['total_earning'], $transit['id'], $ping->id);
            if ($isUpdated) {
                $isUpdated = $this->_updateSellerBalanceOnLeadSold($sellerBalance, $soldData['seller_earning'], $transit['id'], $ping->id);
            }
        }

        return $isUpdated;
    }

    /**
     * Update balance at seller on execute payment
     *
     * !!! WARNING !!! METHOD MUST BE CALL ONLY INSIDE DB TRANSACTION!!!
     * !!! WARNING !!! Logs will save only if outer transaction will be commit
     *
     * @param int $sellerId
     * @param float $payPrice
     * @param int $currencyId
     * @param int $paymentId
     * @return bool
     */
    public function updateBalanceOnPayment($sellerId, $payPrice, $currencyId, $paymentId)
    {
        $isUpdated = false;

        $balances = new Balances();
        $sellerBalance = $balances->getBalanceForUser($sellerId, $currencyId, true, true); /** @var \payments\models\active_records\Balance $sellerBalance */

        if (!empty($sellerBalance)) {
            $isQuerySuccess = false;
            $errorMessage   = null;
            try {
                $isQuerySuccess = (bool)\Yii::app()->db->createCommand(
                    'UPDATE ' . Balance::model()->tableName() .
                    ' SET balance = balance - ' . floatval($payPrice) .
                    ', total_paid = total_paid - ' . floatval($payPrice) .
                    ', last_update_datetime = "' . date('Y-m-d H:i:s') . '"' .
                    ' WHERE id = :id'
                )->execute([':id' => $sellerBalance->id]);
            } catch(\Exception $e) {
                $errorMessage = $e->getMessage();
            }

            // Add to log
            $this->addLog(
                $sellerBalance, // balance
                LogBalanceChange::LOG_TYPE_PAYMENT_FOR_SELLER, // logType
                ($isQuerySuccess ? LogBalanceChange::RESULT_TYPE_SUCCESS : LogBalanceChange::RESULT_TYPE_ERROR), // resultType
                (0 - $payPrice), // moneyForUpdate
                null, // pingId
                null, // transitId
                $paymentId, // paymentId
                null, // invoiceId
                $errorMessage, // comment
                false // isAddToStack
            );

            $isQuerySuccess && $isUpdated = true;
        }

        return $isUpdated;
    }

    /**
     * Update balance at buyer on executed invoice
     *
     * !!! WARNING !!! METHOD MUST BE CALL ONLY INSIDE DB TRANSACTION!!!
     * !!! WARNING !!! Logs will save only if outer transaction will be commit
     *
     * @param int $buyerId
     * @param float $payPrice
     * @param int $currencyId
     * @param int $invoiceId
     * @return bool
     */
    public function updateBalanceOnInvoice($buyerId, $payPrice, $currencyId, $invoiceId)
    {
        $isUpdated = false;

        $balances = new Balances();
        $buyerBalance = $balances->getBalanceForUser($buyerId, $currencyId, true, true); /** @var \payments\models\active_records\Balance $buyerBalance */

        if (!empty($buyerBalance)) {
            $isQuerySuccess = false;
            $errorMessage   = null;
            try {
                $isQuerySuccess = (bool)\Yii::app()->db->createCommand(
                    'UPDATE ' . Balance::model()->tableName() .
                    ' SET balance = balance + ' . floatval($payPrice) .
                    ', total_paid = total_paid + ' . floatval($payPrice) .
                    ', last_update_datetime = "' . date('Y-m-d H:i:s') . '"' .
                    ' WHERE id = :id'
                )->execute([':id' => $buyerBalance->id]);
            } catch(\Exception $e) {
                $errorMessage = $e->getMessage();
            }

            // Add to log
            $this->addLog(
                $buyerBalance, // balance
                LogBalanceChange::LOG_TYPE_INVOICE_FOR_BUYER, // logType
                ($isQuerySuccess ? LogBalanceChange::RESULT_TYPE_SUCCESS : LogBalanceChange::RESULT_TYPE_ERROR), // resultType
                $payPrice, // moneyForUpdate
                null, // pingId
                null, // transitId
                null, // paymentId
                $invoiceId, // invoiceId
                $errorMessage, // comment
                false // isAddToStack
            );

            $isQuerySuccess && $isUpdated = true;
        }

        return $isUpdated;
    }

    /**
     * Update balance at seller on delete payment
     *
     * !!! WARNING !!! METHOD MUST BE CALL ONLY INSIDE DB TRANSACTION!!!
     * !!! WARNING !!! Logs will save only if outer transaction will be commit
     *
     * @param int $sellerId
     * @param float $paidPrice
     * @param int $currencyId
     * @param int $paymentId
     * @return bool
     */
    public function updateBalanceOnPaymentDelete($sellerId, $paidPrice, $currencyId, $paymentId)
    {
        $isUpdated = false;

        $balances = new Balances();
        $sellerBalance = $balances->getBalanceForUser($sellerId, $currencyId, true, true); /** @var \payments\models\active_records\Balance $sellerBalance */

        if (!empty($sellerBalance)) {
            $isQuerySuccess = false;
            $errorMessage   = null;
            try {
                $isQuerySuccess = (bool)\Yii::app()->db->createCommand(
                    'UPDATE ' . Balance::model()->tableName() .
                    ' SET balance = balance + ' . floatval($paidPrice) . // on delete payment we return on seller balance price sum what was paid
                    ', total_paid = total_paid + ' . floatval($paidPrice) .
                    ', last_update_datetime = "' . date('Y-m-d H:i:s') . '"' .
                    ' WHERE id = :id'
                )->execute([':id' => $sellerBalance->id]);
            } catch(\Exception $e) {
                $errorMessage = $e->getMessage();
            }

            // Add to log
            $this->addLog(
                $sellerBalance, // balance
                LogBalanceChange::LOG_TYPE_DELETE_SELLER_PAYMENT, // logType
                ($isQuerySuccess ? LogBalanceChange::RESULT_TYPE_SUCCESS : LogBalanceChange::RESULT_TYPE_ERROR), // resultType
                $paidPrice, // moneyForUpdate
                null, // pingId
                null, // transitId
                $paymentId, // paymentId
                null, // invoiceId
                $errorMessage, // comment
                false // isAddToStack
            );

            $isQuerySuccess && $isUpdated = true;
        }

        return $isUpdated;
    }

    /**
     * Update balance at buyer on delete invoice
     *
     * !!! WARNING !!! METHOD MUST BE CALL ONLY INSIDE DB TRANSACTION!!!
     * !!! WARNING !!! Logs will save only if outer transaction will be commit
     *
     * @param int $buyerId
     * @param float $paidPrice
     * @param int $currencyId
     * @param int $invoiceId
     * @return bool
     */
    public function updateBalanceOnInvoiceDelete($buyerId, $paidPrice, $currencyId, $invoiceId)
    {
        $isUpdated = false;

        $balances = new Balances();
        $buyerBalance = $balances->getBalanceForUser($buyerId, $currencyId, true, true); /** @var \payments\models\active_records\Balance $buyerBalance */

        if (!empty($buyerBalance)) {
            $isQuerySuccess = false;
            $errorMessage   = null;
            try {
                $isQuerySuccess = (bool)\Yii::app()->db->createCommand(
                    'UPDATE ' . Balance::model()->tableName() .
                    ' SET balance = balance - ' . floatval($paidPrice) . // on delete invoice we remove price from buyer balance, what he was paid
                    ', total_paid = total_paid - ' . floatval($paidPrice) .
                    ', last_update_datetime = "' . date('Y-m-d H:i:s') . '"' .
                    ' WHERE id = :id'
                )->execute([':id' => $buyerBalance->id]);
            } catch(\Exception $e) {
                $errorMessage = $e->getMessage();
            }

            // Add to log
            $this->addLog(
                $buyerBalance, // balance
                LogBalanceChange::LOG_TYPE_DELETE_BUYER_INVOICE, // logType
                ($isQuerySuccess ? LogBalanceChange::RESULT_TYPE_SUCCESS : LogBalanceChange::RESULT_TYPE_ERROR), // resultType
                (0 - $paidPrice), // moneyForUpdate
                null, // pingId
                null, // transitId
                null, // paymentId
                $invoiceId, // invoiceId
                $errorMessage, // comment
                false // isAddToStack
            );

            $isQuerySuccess && $isUpdated = true;
        }

        return $isUpdated;
    }



    /**
     * Update BUYER balance when lead sold
     *
     * @param Balance $balance
     * @param float $buyerPaid
     * @param int $transitId
     * @param int $pingId
     * @return bool
     */
    protected function _updateBuyerBalanceOnLeadSold(Balance $balance, $buyerPaid, $transitId, $pingId)
    {
        $isUpdated = false;

        $errorMessage = null;
        try {
            $isUpdated = (bool) \Yii::app()->db->createCommand(
                'UPDATE ' . Balance::model()->tableName() .
                ' SET balance = balance - ' . floatval($buyerPaid) .
                ', last_update_datetime = "' . date('Y-m-d H:i:s') . '"' .
                ' WHERE id = :id'
            )->execute([':id' => $balance->id]);
        } catch(\Exception $e) {
            $errorMessage = $e->getMessage();
        }

        // Add to log
        $this->addLog(
            $balance, // balance
            LogBalanceChange::LOG_TYPE_LEAD_BOUGHT_BY_BUYER, // logType
            ($isUpdated ? LogBalanceChange::RESULT_TYPE_SUCCESS : LogBalanceChange::RESULT_TYPE_ERROR), // resultType
            (0 - $buyerPaid), // moneyForUpdate
            $pingId, // pingId
            $transitId, // transitId
            null, // paymentId
            null, // invoiceId
            $errorMessage, // comment
            false // isAddToStack
        );

        return $isUpdated;
    }

    /**
     * Update SELLER balance when lead sold
     *
     * @param Balance $balance
     * @param float $sellerEarning
     * @param int $transitId
     * @param int $pingId
     * @return bool
     */
    protected function _updateSellerBalanceOnLeadSold(Balance $balance, $sellerEarning, $transitId, $pingId)
    {
        $isUpdated = false;

        $errorMessage = null;
        try {
            $isUpdated = (bool)\Yii::app()->db->createCommand(
                'UPDATE ' . Balance::model()->tableName() .
                ' SET balance = balance + ' . floatval($sellerEarning) .
                ', last_update_datetime = "' . date('Y-m-d H:i:s') . '"' .
                ' WHERE id = :id'
            )->execute([':id' => $balance->id]);

        } catch(\Exception $e) {
            $errorMessage = $e->getMessage();
        }

        // Add to log
        $this->addLog(
            $balance, // balance
            LogBalanceChange::LOG_TYPE_LEAD_SOLD_BY_SELLER, // logType
            ($isUpdated ? LogBalanceChange::RESULT_TYPE_SUCCESS : LogBalanceChange::RESULT_TYPE_ERROR), // resultType
            $sellerEarning, // moneyForUpdate
            $pingId, // pingId
            $transitId, // transitId
            null, // paymentId
            null, // invoiceId
            $errorMessage, // comment
            false // isAddToStack
        );

        return $isUpdated;
    }


    /**
     * Save log
     *
     * @param Balance $balance
     * @param int $logType
     * @param int $resultType
     * @param float $moneyForUpdate
     * @param int|null $pingId
     * @param int|null $transitId
     * @param int|null $paymentId
     * @param int|null $invoiceId
     * @param int|null $comment
     * @param bool $isAddToStack
     */
    public function addLog(Balance $balance, $logType, $resultType, $moneyForUpdate, $pingId, $transitId, $paymentId, $invoiceId, $comment = null, $isAddToStack = false)
    {
        if (empty($comment) && $resultType == LogBalanceChange::RESULT_TYPE_ERROR) {
            $params = json_encode([
                'balance' => $balance->getAttributes(),
            ]);
            $comment = "Error on update balance.\n";
            $comment .= "PARAMS: " . $params . " .\n";
            $comment .= "TRACE: " . json_encode(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
        }

        $log = new LogBalanceChange();

        $log->balance_id            = $balance->id;
        $log->creation_datetime     = date('Y-m-d H:i:s');
        $log->log_type              = $logType;
        $log->result_type           = $resultType;
        $log->updated_real_user_id  = null;
        $log->currency_id           = $balance->currency_id;
        $log->money_for_update      = $moneyForUpdate;
        $log->comment               = $comment;
        $log->ping_id               = $pingId;
        $log->transit_id            = $transitId;
        $log->payment_id            = $paymentId;
        $log->invoice_id            = $invoiceId;

        if(!\CoreHelper::isConsoleApp() && \Yii::app()->user) {
            $log->updated_real_user_id = \UsersPermissions::getCurrentUserId(true);
        }

        if ($isAddToStack) {
            $logStack = LogStack::getInstance();
            $logStack->addActiveRecordToStack($log); // then it must be apply
        } else {
            $log->save();
        }
    }

}