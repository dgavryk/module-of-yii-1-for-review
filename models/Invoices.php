<?php namespace payments\models;

use payments\models\active_records\Invoice;
use payments\models\active_records\PaymentMethodName;

class Invoices
{

    protected static $_cache = []; // memorization


    /**
     * Get invoice by id
     *
     * @param int $id
     * @param bool $isObject
     * @param bool $checkAccess
     * @param bool $checkOnRemoved
     * @return array|\payments\models\active_records\Invoice|null
     */
    public function getInvoiceById($id, $isObject = false, $checkAccess = true, $checkOnRemoved = true)
    {
        $cacheKey = __CLASS__ . ';' . __METHOD__ . ';' . implode(';', [$id, $isObject, $checkAccess, $checkOnRemoved]);
        if (!array_key_exists($cacheKey, self::$_cache)) {

            $result = null;
            if (!empty($id)) {
                if ($checkAccess) {
                    $allowUserIds = \UsersPermissions::getAllowUsersIds();
                    if (empty($allowUserIds)) {
                        return $result;
                    }
                } else {
                    $allowUserIds = [];
                }

                if ($isObject) {
                    $whereParams = ['id' => $id];
                    $checkOnRemoved && $whereParams['is_removed'] = 0;
                    $invoice = Invoice::model()->findByAttributes($whereParams); /** @var Invoice $invoice */
                    if ($invoice && (!$checkAccess || in_array($invoice->user_id, $allowUserIds))) {
                        $result = $invoice;
                    }
                } else {
                    $command = \Yii::app()->db->createCommand()
                        ->select('*')
                        ->from(Invoice::model()->tableName())
                        ->where('id = :id', [':id' => $id]);
                    if ($checkOnRemoved) {
                        $command->andWhere('is_removed = 0');
                    }
                    if ($checkAccess) {
                        $command->andWhere(['in', 'user_id', $allowUserIds]);
                    }

                    $result = $command->queryRow();
                }
            }
            self::$_cache[$cacheKey] = !empty($result) ? $result : null;
        }

        return self::$_cache[$cacheKey];
    }

    /**
     * Get list of invoices for user
     *
     * @param array $filters
     * @param int $perPage
     * @param int $pageNumber
     * @return array Example:
     * [
     *      'listData' => [],
     *      'totalItemsCount' => int,
     *      'totalItemsRow' => [],
     * ]
     */
    public function getInvoicesList($filters = [], $perPage = null, $pageNumber = null)
    {
        $command = \Yii::app()->db->createCommand()
            ->select(
                'i.id, i.status, i.pay_date, i.pay_period_from, i.pay_period_to' .
                ', ROUND(i.invoice_sum, 2) AS invoice_sum, ROUND(i.fee_sum, 2) AS fee_sum, ROUND(i.received_sum, 2) AS received_sum' .
                ', u.username AS buyer_name' .
                ', pmn.name AS payment_method_name, c.code AS currency_name'
            )
            ->from(Invoice::model()->tableName() . ' AS i')
            ->leftJoin(\Currency::model()->tableName() . ' AS c', 'c.id = i.currency_id')
            ->leftJoin(PaymentMethodName::model()->tableName() . ' AS pmn', 'pmn.id = i.payment_method_name_id')
            ->leftJoin(\User::model()->tableName() . ' AS u', 'u.id = i.user_id')
            ->where('i.is_removed = 0')
            ->group('i.id')
            ->order('i.id DESC');


        // add filters
        $this->_addInvoicesListWhere($command, $filters);

        // add pagination
        if (!empty($perPage)) {
            $offset = null;
            if (!empty($pageNumber) && $pageNumber > 1) {
                $offset = $perPage * ($pageNumber - 1);
            }
            $command->limit($perPage, $offset);
        }

        $listData           = $command->queryAll();
        $totalItemsCount    = 0;
        $totalItemsRow      = [];

        if (!empty($listData)) {
            $statusList = Invoice::getStatusList(true);
            foreach ($listData as $k => $v) {
                $listData[$k]['status_name'] = isset($statusList[$v['status']]) ? $statusList[$v['status']] : '';
            }

            $total = $this->_getInvoicesListTotal($command);
            !empty($total['totalItems'])    && $totalItemsCount = $total['totalItems'];
            !empty($total['totalRow'])      && $totalItemsRow = $total['totalRow'];
        }

        $result = [
            'listData' => $listData,
            'totalItemsCount' => $totalItemsCount,
            'totalItemsRow' => $totalItemsRow,
        ];

        return $result;
    }

    /**
     * Add WHERE conditions to list command
     *
     * @param \CDbCommand $command
     * @param array $filters
     */
    protected function _addInvoicesListWhere(\CDbCommand $command, $filters)
    {
        if (!empty($filters['payment_method_name_id'])) {
            $command->andWhere('i.payment_method_name_id = :payment_method_name_id', [':payment_method_name_id' => $filters['payment_method_name_id']]);
        }
        if (!empty($filters['currency_id'])) {
            $command->andWhere('i.currency_id = :currency_id', [':currency_id' => $filters['currency_id']]);
        }
        if (!empty($filters['status'])) {
            $command->andWhere('i.status = :status', [':status' => $filters['status']]);
        }
        if (!empty($filters['pay_date_from'])) {
            $command->andWhere('i.pay_date >= :pay_date_from', [':pay_date_from' => $filters['pay_date_from']]);
        }
        if (!empty($filters['pay_date_to'])) {
            $command->andWhere('i.pay_date <= :pay_date_to', [':pay_date_to' => $filters['pay_date_to']]);
        }
        if (\UsersPermissions::getCurrentUserRoleId(true) == \User::ROLE_ADMIN) {
            if (!empty($filters['seller_id'])) {
                $command->andWhere('i.user_id = :user_id', [':user_id' => $filters['seller_id']]);
            }
        } else {
            $command->andWhere('i.user_id = :user_id', [':user_id' => \UsersPermissions::getCurrentUserId(true)]);
        }
    }

    /**
     * Get TOTAL row for list command
     *
     * @param \CDbCommand $command
     * @return array Example:
     * [
     *      'totalItems' => int,
     *      'totalRow' => array,
     * ]
     */
    protected function _getInvoicesListTotal(\CDbCommand $command)
    {
        $dataTotal = [
            'totalItems' => 0,
            'totalRow' => [],
        ];

        $totalCommand = clone $command;

        $totalCommand->setText(''); // for don't use Yii cache query from previous command execute
        $totalCommand->select('COUNT(DISTINCT i.id) AS total_count_rows, ROUND(SUM(i.invoice_sum), 2) AS invoice_sum, ROUND(SUM(i.fee_sum), 2) AS fee_sum, ROUND(SUM(i.received_sum), 2) AS received_sum');
        $totalCommand->limit(-1); // unset limit
        $totalCommand->offset(-1); // unset offset
        $totalCommand->group = null;

        $total = $totalCommand->queryRow();

        if (!empty($total)) {
            $dataTotal['totalItems'] = $total['total_count_rows'];

            unset($total['total_count_rows']);
            $dataTotal['totalRow'] = $total;
        }

        return $dataTotal;
    }


}