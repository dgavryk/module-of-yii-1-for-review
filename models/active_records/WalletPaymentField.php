<?php namespace payments\models\active_records;

/**
 * This is the model class for table "wallet_payment_fields".
 *
 * The followings are the available columns in table 'wallet_payment_fields':
 * @property int id
 * @property int wallet_id
 * @property int payment_method_field_id
 * @property string field_value
 */
class WalletPaymentField extends BaseActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wallet_payment_fields';
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return WalletPaymentField the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}