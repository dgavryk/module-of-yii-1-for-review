<?php namespace payments\models\active_records;

/**
 * This is the model class for table "payment_method_fields".
 *
 * The followings are the available columns in table 'payment_method_fields':
 * @property int id
 * @property int payment_method_id
 * @property string field_attribute
 * @property string field_name
 * @property string field_description
 * @property int field_type
 * @property string extra_params
 */
class PaymentMethodField extends BaseActiveRecord
{
    const FIELD_TYPE_DEFAULT_STRING     = 1;


    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'payment_method_fields';
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PaymentMethodField the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Get name of view for field
     *
     * @param int $fieldType
     * @return string|null
     */
    public static function getPaymentMethodFieldViewName($fieldType)
    {
        $result = null;

        $viewNames = [
            self::FIELD_TYPE_DEFAULT_STRING => 'defaultStringField',
        ];


        if (isset($viewNames[$fieldType])) {
            $result = $viewNames[$fieldType];
        }

        return $result;
    }

}