<?php namespace payments\models\active_records;

/**
 * This is the model class for table "wallets".
 *
 * The followings are the available columns in table 'wallets':
 * @property int id
 * @property int user_id
 * @property int payment_method_id
 * @property string name
 * @property int status
 * @property int is_removed
 */
class Wallet extends BaseActiveRecord
{

    const STATUS_ACTIVE         = 1;
    const STATUS_NOT_ACTIVE     = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'wallets';
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Wallet the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Get statuses list
     *
     * @param bool $withNames
     * @return array
     */
    public static function getStatusList($withNames = true)
    {
        $statuses = [
            static::STATUS_ACTIVE       => \Yii::t('all', 'status_active'),
            static::STATUS_NOT_ACTIVE   => \Yii::t('all', 'status_not_active'),
        ];

        if (!$withNames) {
            $statuses = array_keys($statuses);
        }

        return $statuses;
    }

}