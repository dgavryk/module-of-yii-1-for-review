<?php namespace payments\models\active_records;

/**
 * This is the model class for table "balances".
 *
 * The followings are the available columns in table 'balances':
 * @property int id
 * @property int user_id
 * @property int currency_id
 * @property float balance
 * @property float total_paid
 * @property string creation_datetime
 * @property string last_update_datetime
 */
class Balance extends BaseActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'balances';
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Balance the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}