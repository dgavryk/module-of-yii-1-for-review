<?php namespace payments\models\active_records;

/**
 * This is the model class for table "payments".
 *
 * The followings are the available columns in table 'payments':
 * @property int id
 * @property string creation_datetime
 * @property int user_id
 * @property int wallet_id
 * @property int status
 * @property string pay_date
 * @property string pay_period_from
 * @property string pay_period_to
 * @property float paid_sum
 * @property float fee_sum
 * @property float received_sum
 * @property string comment
 * @property int is_removed
 */
class Payment extends BaseActiveRecord
{

    const STATUS_PAID          = 1;
    const STATUS_NOT_PAID      = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'payments';
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Payment the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Get statuses list
     *
     * @param bool $withNames
     * @return array
     */
    public static function getStatusList($withNames = true)
    {
        $statuses = [
            static::STATUS_PAID       => \Yii::t('all', 'paid'),
            static::STATUS_NOT_PAID   => \Yii::t('all', 'not_paid'),
        ];

        if (!$withNames) {
            $statuses = array_keys($statuses);
        }

        return $statuses;
    }

}