<?php namespace payments\models\active_records;

/**
 * This is the model class for table "payment_method_names".
 *
 * The followings are the available columns in table 'payment_method_names':
 * @property int id
 * @property int name
 */
class PaymentMethodName extends BaseActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'payment_method_names';
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PaymentMethodName the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}