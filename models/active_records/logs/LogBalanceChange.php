<?php namespace payments\models\active_records\logs;

use payments\models\active_records\BaseActiveRecord;

/**
 * This is the model class for table "log_balances_changes".
 *
 * The followings are the available columns in table 'log_balances_changes':
 * @property int id
 * @property int balance_id
 * @property string creation_datetime
 * @property int log_type
 * @property int result_type
 * @property int updated_real_user_id
 * @property int currency_id
 * @property float money_for_update
 * @property int comment
 * @property int ping_id
 * @property int transit_id
 * @property int payment_id
 * @property int invoice_id
 */
class LogBalanceChange extends BaseActiveRecord
{

    const LOG_TYPE_INVOICE_FOR_BUYER            = 1; // пополнение баланса рекламодателя (в связи с оплатой счёта) (создан счёт и он оплачен)
    const LOG_TYPE_PAYMENT_FOR_SELLER           = 2; // снятие средств с баланса вебмастера (и перевод ему на счёт) (создана выплата и она выплачена)
    const LOG_TYPE_LEAD_BOUGHT_BY_BUYER         = 3; // лид куплен рекламодателем (снятие средств за покупку лида)
    const LOG_TYPE_LEAD_SOLD_BY_SELLER          = 4; // лид продан вебмастером (пополнение средств за продажу лида)
    const LOG_TYPE_DELETE_BUYER_INVOICE         = 5; // удаление оплаченого счёта (снимаем средства обратно с баланса на ту сумму, которая была в счёте)
    const LOG_TYPE_DELETE_SELLER_PAYMENT        = 6; // удаление выплаченой выплаты (пополняем обратно баланс на ту сумму, которая была в выплате)


    const RESULT_TYPE_SUCCESS   = 1;
    const RESULT_TYPE_ERROR     = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'log_balances_changes';
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LogBalanceChange the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}