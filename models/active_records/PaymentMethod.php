<?php namespace payments\models\active_records;

/**
 * This is the model class for table "payment_methods".
 *
 * The followings are the available columns in table 'payment_methods':
 * @property int id
 * @property int payment_method_name_id
 * @property int min_pay_out
 * @property string fee_description
 * @property int currency_id
 * @property int is_removed
 */
class PaymentMethod extends BaseActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'payment_methods';
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PaymentMethod the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}