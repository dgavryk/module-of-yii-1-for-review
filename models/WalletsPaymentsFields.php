<?php namespace payments\models;

use payments\models\active_records\PaymentMethodField;
use payments\models\active_records\WalletPaymentField;

class WalletsPaymentsFields
{

    /**
     * Get wallet payments data
     *
     * @param int $walletId
     * @return array
     */
    public function getWalletPaymentFieldsData($walletId)
    {
        $result = [];

        if (!empty($walletId)) {
            $fieldsData = \Yii::app()->db->createCommand()
                ->select('wpf.*, pmf.field_attribute, pmf.field_name AS field_name')
                ->from(WalletPaymentField::model()->tableName() . ' AS wpf')
                ->join(PaymentMethodField::model()->tableName() . ' AS pmf', 'pmf.id = wpf.payment_method_field_id')
                ->where('wpf.wallet_id = :wallet_id', [':wallet_id' => $walletId])
                ->queryAll();

            !empty($fieldsData) && $result = $fieldsData;
        }

        return $result;
    }

    /**
     * @param int $walletId
     * @param bool $isKeyFieldAttribute (in result array key will be attribute name or payment field id)
     * @return array
     */
    public function getWalletPaymentFieldsValues($walletId, $isKeyFieldAttribute = true)
    {
        $result = [];

        if (!empty($walletId)) {
            $fieldsValues = $this->getWalletPaymentFieldsData($walletId);

            foreach($fieldsValues as $v) {
                $key = $isKeyFieldAttribute ? $v['field_attribute'] : $v['payment_method_field_id'];
                $result[$key] = $v['field_value'];
            }
        }

        return $result;
    }

}