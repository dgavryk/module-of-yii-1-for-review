<?php namespace payments\models;

use payments\models\active_records\PaymentMethodName;

class PaymentMethodsNames
{

    // WARNING! Be careful with cash, data can be updated but in cash will be old data
    protected static $_cache = []; // memorization


    /**
     * Get list of all payment methods names
     *
     * @return array
     */
    public static function getAllPaymentMethodNames()
    {
        $cacheKey = __CLASS__ . ';' . __METHOD__;

        if (!array_key_exists($cacheKey, self::$_cache)) {
            $result = \Yii::app()->db->createCommand()
                ->select('*')
                ->from(PaymentMethodName::model()->tableName())
                ->queryAll();

            foreach($result as $v) {
                self::$_cache[$cacheKey][$v['id']] = $v;
            }
        }

        return self::$_cache[$cacheKey];
    }

    /**
     * Get name of payment method by id
     *
     * @param int $id
     * @return string|null
     */
    public static function getNameById($id)
    {
        $allPaymentsNames = static::getAllPaymentMethodNames();

        $name = isset($allPaymentsNames[$id]) ? $allPaymentsNames[$id]['name'] : null;

        return $name;
    }

}