<?php namespace payments\models;

use payments\models\active_records\Payment;
use payments\models\active_records\PaymentMethod;
use payments\models\active_records\PaymentMethodName;
use payments\models\active_records\Wallet;

class Payments
{

    protected static $_cache = []; // memorization


    /**
     * Get payment by id
     *
     * @param int $id
     * @param bool $isObject
     * @param bool $checkAccess
     * @param bool $checkOnRemoved
     * @return array|\payments\models\active_records\Payment|null
     */
    public function getPaymentById($id, $isObject = false, $checkAccess = true, $checkOnRemoved = true)
    {
        $cacheKey = __CLASS__ . ';' . __METHOD__ . ';' . implode(';', [$id, $isObject, $checkAccess, $checkOnRemoved]);
        if (!array_key_exists($cacheKey, self::$_cache)) {

            $result = null;
            if (!empty($id)) {
                if ($checkAccess) {
                    $allowUserIds = \UsersPermissions::getAllowUsersIds();
                    if (empty($allowUserIds)) {
                        return $result;
                    }
                } else {
                    $allowUserIds = [];
                }

                if ($isObject) {
                    $whereParams = ['id' => $id];
                    $checkOnRemoved && $whereParams['is_removed'] = 0;
                    $payment = Payment::model()->findByAttributes($whereParams); /** @var Payment $payment */
                    if ($payment && (!$checkAccess || in_array($payment->user_id, $allowUserIds))) {
                        $result = $payment;
                    }
                } else {
                    $command = \Yii::app()->db->createCommand()
                        ->select('*')
                        ->from(Payment::model()->tableName())
                        ->where('id = :id', [':id' => $id]);
                    if ($checkOnRemoved) {
                        $command->andWhere('is_removed = 0');
                    }
                    if ($checkAccess) {
                        $command->andWhere(['in', 'user_id', $allowUserIds]);
                    }

                    $result = $command->queryRow();
                }
            }
            self::$_cache[$cacheKey] = !empty($result) ? $result : null;
        }

        return self::$_cache[$cacheKey];
    }

    /**
     * Get list of payments for user
     *
     * @param array $filters
     * @param int $perPage
     * @param int $pageNumber
     * @return array Example:
     * [
     *      'listData' => [],
     *      'totalItemsCount' => int,
     *      'totalItemsRow' => [],
     * ]
     */
    public function getPaymentsList($filters = [], $perPage = null, $pageNumber = null)
    {
        $command = \Yii::app()->db->createCommand()
            ->select(
                'p.id, p.status, p.pay_date, p.pay_period_from, p.pay_period_to' .
                ', ROUND(p.paid_sum, 2) AS paid_sum, ROUND(p.fee_sum, 2) AS fee_sum, ROUND(p.received_sum, 2) AS received_sum' .
                ', u.username AS seller_name, w.name AS wallet_name' .
                ', pmn.name AS payment_method_name, c.code AS currency_name'
            )
            ->from(Payment::model()->tableName() . ' AS p')
            ->leftJoin(Wallet::model()->tableName() . ' AS w', 'w.id = p.wallet_id')
            ->leftJoin(PaymentMethod::model()->tableName() . ' AS pm', 'pm.id = w.payment_method_id')
            ->leftJoin(\Currency::model()->tableName() . ' AS c', 'c.id = pm.currency_id')
            ->leftJoin(PaymentMethodName::model()->tableName() . ' AS pmn', 'pmn.id = pm.payment_method_name_id')
            ->leftJoin(\User::model()->tableName() . ' AS u', 'u.id = p.user_id')
            ->where('p.is_removed = 0')
            ->group('p.id')
            ->order('p.id DESC');


        // add filters
        $this->_addPaymentsListWhere($command, $filters);

        // add pagination
        if (!empty($perPage)) {
            $offset = null;
            if (!empty($pageNumber) && $pageNumber > 1) {
                $offset = $perPage * ($pageNumber - 1);
            }
            $command->limit($perPage, $offset);
        }

        $listData           = $command->queryAll();
        $totalItemsCount    = 0;
        $totalItemsRow      = [];

        if (!empty($listData)) {
            $statusList = Payment::getStatusList(true);
            foreach ($listData as $k => $v) {
                $listData[$k]['status_name'] = isset($statusList[$v['status']]) ? $statusList[$v['status']] : '';
            }

            $total = $this->_getPaymentsListTotal($command);
            !empty($total['totalItems'])    && $totalItemsCount = $total['totalItems'];
            !empty($total['totalRow'])      && $totalItemsRow = $total['totalRow'];
        }

        $result = [
            'listData' => $listData,
            'totalItemsCount' => $totalItemsCount,
            'totalItemsRow' => $totalItemsRow,
        ];

        return $result;
    }

    /**
     * Add WHERE conditions to list command
     *
     * @param \CDbCommand $command
     * @param array $filters
     */
    protected function _addPaymentsListWhere(\CDbCommand $command, $filters)
    {
        if (!empty($filters['payment_method_name_id'])) {
            $command->andWhere('pm.payment_method_name_id = :payment_method_name_id', [':payment_method_name_id' => $filters['payment_method_name_id']]);
        }
        if (!empty($filters['currency_id'])) {
            $command->andWhere('pm.currency_id = :currency_id', [':currency_id' => $filters['currency_id']]);
        }
        if (!empty($filters['status'])) {
            $command->andWhere('p.status = :status', [':status' => $filters['status']]);
        }
        if (!empty($filters['pay_date_from'])) {
            $command->andWhere('p.pay_date >= :pay_date_from', [':pay_date_from' => $filters['pay_date_from']]);
        }
        if (!empty($filters['pay_date_to'])) {
            $command->andWhere('p.pay_date <= :pay_date_to', [':pay_date_to' => $filters['pay_date_to']]);
        }
        if (\UsersPermissions::getCurrentUserRoleId(true) == \User::ROLE_ADMIN) {
            if (!empty($filters['seller_id'])) {
                $command->andWhere('w.user_id = :user_id', [':user_id' => $filters['seller_id']]);
            }
        } else {
            $command->andWhere('w.user_id = :user_id', [':user_id' => \UsersPermissions::getCurrentUserId(true)]);
        }
    }

    /**
     * Get TOTAL row for list command
     *
     * @param \CDbCommand $command
     * @return array Example:
     * [
     *      'totalItems' => int,
     *      'totalRow' => array,
     * ]
     */
    protected function _getPaymentsListTotal(\CDbCommand $command)
    {
        $dataTotal = [
            'totalItems' => 0,
            'totalRow' => [],
        ];

        $totalCommand = clone $command;

        $totalCommand->setText(''); // for don't use Yii cache query from previous command execute
        $totalCommand->select('COUNT(DISTINCT p.id) AS total_count_rows, ROUND(SUM(p.paid_sum), 2) AS paid_sum, ROUND(SUM(p.fee_sum), 2) AS fee_sum, ROUND(SUM(p.received_sum), 2) AS received_sum');
        $totalCommand->limit(-1); // unset limit
        $totalCommand->offset(-1); // unset offset
        $totalCommand->group = null;

        $total = $totalCommand->queryRow();

        if (!empty($total)) {
            $dataTotal['totalItems'] = $total['total_count_rows'];

            unset($total['total_count_rows']);
            $dataTotal['totalRow'] = $total;
        }

        return $dataTotal;
    }


}