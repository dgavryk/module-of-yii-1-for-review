<?php namespace payments\models;

use payments\models\active_records\PaymentMethod;
use payments\models\active_records\PaymentMethodName;
use payments\models\active_records\Wallet;

class Wallets
{

    protected static $_cache = []; // memorization


    /**
     * Get wallet by id
     *
     * @param int $id
     * @param bool $isObject
     * @param bool $checkAccess
     * @param bool $checkOnRemoved
     * @return array|\payments\models\active_records\Wallet|null
     */
    public function getWalletById($id, $isObject = false, $checkAccess = true, $checkOnRemoved = true)
    {
        $cacheKey = __CLASS__ . ';' . __METHOD__ . ';' . implode(';', [$id, $isObject, $checkAccess, $checkOnRemoved]);
        if (!array_key_exists($cacheKey, self::$_cache)) {

            $result = null;
            if (!empty($id)) {
                if ($checkAccess) {
                    $allowUserIds = \UsersPermissions::getAllowUsersIds();
                    if (empty($allowUserIds)) {
                        return $result;
                    }
                } else {
                    $allowUserIds = [];
                }

                if ($isObject) {
                    $whereParams = ['id' => $id];
                    $checkOnRemoved && $whereParams['is_removed'] = 0;
                    $wallet = Wallet::model()->findByAttributes($whereParams); /** @var Wallet $wallet */
                    if ($wallet && (!$checkAccess || in_array($wallet->user_id, $allowUserIds))) {
                        $result = $wallet;
                    }
                } else {
                    $command = \Yii::app()->db->createCommand()
                        ->select('*')
                        ->from(Wallet::model()->tableName())
                        ->where('id = :id', [':id' => $id]);
                    if ($checkOnRemoved) {
                        $command->andWhere('is_removed = 0');
                    }
                    if ($checkAccess) {
                        $command->andWhere(['in', 'user_id', $allowUserIds]);
                    }

                    $result = $command->queryRow();
                }
            }
            self::$_cache[$cacheKey] = !empty($result) ? $result : null;
        }

        return self::$_cache[$cacheKey];
    }

    /**
     * Get list of wallets for user
     *
     * @param array $filters
     * @return array
     */
    public function getWalletsList($filters = [])
    {
        $command = \Yii::app()->db->createCommand()
            ->select('w.*, pmn.name AS payment_method_name, c.code AS currency_name')
            ->from(Wallet::model()->tableName() . ' AS w')
            ->leftJoin(PaymentMethod::model()->tableName() . ' AS pm', 'pm.id = w.payment_method_id')
            ->leftJoin(\Currency::model()->tableName() . ' AS c', 'c.id = pm.currency_id')
            ->leftJoin(PaymentMethodName::model()->tableName() . ' AS pmn', 'pmn.id = pm.payment_method_name_id')
            ->where('w.is_removed = 0')
            ->order('w.id DESC');

        $command->andWhere('w.user_id = :user_id', [':user_id' => \UsersPermissions::getCurrentUserId()]);

        if (!empty($filters['currency_id'])) {
            $command->andWhere('pm.currency_id = :currency_id', [':currency_id' => $filters['currency_id']]);
        }
        if (!empty($filters['status'])) {
            $command->andWhere('w.status = :status', [':status' => $filters['status']]);
        }

        $result = $command->queryAll();

        if (!empty($result)) {
            $statusList = Wallet::getStatusList(true);
            foreach ($result as $k => $v) {
                $result[$k]['status_name'] = isset($statusList[$v['status']]) ? $statusList[$v['status']] : '';
            }
        }

        return $result;
    }

    /**
     * Get active wallet for user
     *
     * @param int $userId
     * @param int $currencyId
     * @return array
     */
    public function getUserActiveWallet($userId, $currencyId)
    {
        $result = [];

        if (!empty($userId) && !empty($currencyId)) {
            $command = \Yii::app()->db->createCommand()
                ->select('w.*, pmn.name AS payment_method_name, c.code AS currency_name')
                ->from(Wallet::model()->tableName() . ' AS w')
                ->leftJoin(PaymentMethod::model()->tableName() . ' AS pm', 'pm.id = w.payment_method_id')
                ->leftJoin(\Currency::model()->tableName() . ' AS c', 'c.id = pm.currency_id')
                ->leftJoin(PaymentMethodName::model()->tableName() . ' AS pmn', 'pmn.id = pm.payment_method_name_id')
                ->where('w.is_removed = 0')
                ->andWhere(
                    'pm.currency_id = :currency_id AND w.user_id = :user_id',
                    [':currency_id' => $currencyId, ':user_id' => $userId]
                )
                ->andWhere('w.status = :status', [':status' => Wallet::STATUS_ACTIVE]);

            $wallet = $command->queryRow();

            if (!empty($wallet)) {
                $walletsPaymentsFields = new WalletsPaymentsFields();

                $result = $wallet;
                $result['payment_fields'] = $walletsPaymentsFields->getWalletPaymentFieldsData($wallet['id']);
            }
        }

        return $result;
    }

    /**
     * Get wallet data
     *
     * @param int $walletId
     * @return array
     */
    public function getWalletData($walletId)
    {
        $result = [];

        if (!empty($walletId)) {
            $command = \Yii::app()->db->createCommand()
                ->select('w.*, pm.currency_id AS currency_id, pmn.name AS payment_method_name, c.code AS currency_name')
                ->from(Wallet::model()->tableName() . ' AS w')
                ->leftJoin(PaymentMethod::model()->tableName() . ' AS pm', 'pm.id = w.payment_method_id')
                ->leftJoin(\Currency::model()->tableName() . ' AS c', 'c.id = pm.currency_id')
                ->leftJoin(PaymentMethodName::model()->tableName() . ' AS pmn', 'pmn.id = pm.payment_method_name_id')
                ->andWhere('w.id = :wallet_id', [':wallet_id' => $walletId]);

            $wallet = $command->queryRow();

            if (!empty($wallet)) {
                $walletsPaymentsFields = new WalletsPaymentsFields();

                $result = $wallet;
                $result['payment_fields'] = $walletsPaymentsFields->getWalletPaymentFieldsData($wallet['id']);
            }
        }

        return $result;
    }

}