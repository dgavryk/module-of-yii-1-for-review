<?php namespace payments\models;

use payments\models\active_records\PaymentMethodField;

class PaymentsMethodsFields
{

    /**
     * Get list of fields for payment method
     *
     * @param int $paymentMethodId
     * @return array
     */
    public function getFieldsForPaymentMethod($paymentMethodId)
    {
        $fields = [];

        if (!empty($paymentMethodId)) {
            $fields = \Yii::app()->db->createCommand()
                ->select('*')
                ->from(PaymentMethodField::model()->tableName())
                ->where('payment_method_id = :payment_method_id', [':payment_method_id' => $paymentMethodId])
                ->order('id')
                ->queryAll();

            foreach($fields as $k => $v) {
                if (!empty($v['extra_params'])) {
                    $fields[$k]['extra_params'] = json_decode($v['extra_params'], true);
                }
            }
        }

        return $fields;
    }

}