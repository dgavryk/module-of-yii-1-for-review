<?php namespace payments\models;

use payments\models\active_records\Balance;

class Balances
{

    // WARNING! Be careful with cash, data can be updated but in cash will be old data
    protected static $_cache = []; // memorization


    /**
     * Get user balances
     *
     * @param int $userId
     * @param bool $checkAccess
     * @return array
     */
    public function getUserAllBalances($userId, $checkAccess = true)
    {
        $result = [];

        if (!empty($userId)) {
            $command = \Yii::app()->db->createCommand()
                ->select('*')
                ->from(Balance::model()->tableName())
                ->where('user_id = :user_id', [':user_id' => $userId]);

            if ($checkAccess) {
                $allowUserIds = \UsersPermissions::getAllowUsersIds();
                if (empty($allowUserIds)) {
                    return $result;
                }
                $command->andWhere(['in', 'user_id', $allowUserIds]);
            }

            $result = $command->queryAll();
        }

        return $result;
    }

    /**
     * Get balances for current user
     *
     * @param bool $isRealUser
     * @return array
     */
    public static function getCurrentUserAllBalances($isRealUser = false)
    {
        $balances = self::getUserAllBalances(\UsersPermissions::getCurrentUserId($isRealUser), true);

        return $balances;
    }


    /**
     * Get balance for user
     *
     * @param int $userId
     * @param int $currencyId
     * @param bool $isObject
     * @param bool $createNewOnEmpty
     * @return array|null|\payments\models\active_records\Balance
     */
    public function getBalanceForUser($userId, $currencyId, $isObject = true, $createNewOnEmpty = true)
    {
        $balance = null;

        if (!empty($userId) && !empty($currencyId)) {

            if ($isObject) { // get like Object
                $whereParams = ['user_id' => $userId, 'currency_id' => $currencyId];
                $existedBalance = Balance::model()->findByAttributes($whereParams); /** @var Balance $existedBalance */
                if ($createNewOnEmpty && empty($existedBalance)) {
                    $newBalance = $this->_createNewBalance($userId, $currencyId);
                    !empty($newBalance) && $existedBalance = $newBalance;
                }
            } else { // get like Array
                $existedBalance = $this->_getBalanceByUserAndCurrency($userId, $currencyId); /** @var array $existedBalance */
                if ($createNewOnEmpty && empty($existedBalance)) {
                    $newBalance = $this->_createNewBalance($userId, $currencyId);
                    if (!empty($newBalance)) {
                        $existedBalance = $this->_getBalanceByUserAndCurrency($userId, $currencyId);
                    }
                }
            }

            !empty($existedBalance) && $balance = $existedBalance;
        }

        return $balance;
    }

    /**
     * Get balance by user id and currency (it's unique key)
     *
     * @param int $userId
     * @param int $currencyId
     * @return array
     */
    protected function _getBalanceByUserAndCurrency($userId, $currencyId)
    {
        $balance = [];

        $existedBalance = \Yii::app()->db->createCommand()
            ->select('*')
            ->from(Balance::model()->tableName())
            ->where('user_id = :user_id AND currency_id = :currency_id', [':user_id' => $userId, ':currency_id' => $currencyId])
            ->queryRow();

        !empty($existedBalance) && $balance = $existedBalance;

        return $balance;
    }

    /**
     * Create new balance
     *
     * @param int $userId
     * @param int $currencyId
     * @return null|Balance
     */
    protected function _createNewBalance($userId, $currencyId)
    {
        $balance = null;

        if (!empty($userId) && !empty($currencyId)) {
            $balance = new Balance();

            $balance->user_id               = $userId;
            $balance->currency_id           = $currencyId;
            $balance->balance               = 0;
            $balance->total_paid            = 0;
            $balance->creation_datetime     = date('Y-m-d H:i:s');
            $balance->last_update_datetime  = null;

            $balance->save();
        }

        return $balance;
    }
}